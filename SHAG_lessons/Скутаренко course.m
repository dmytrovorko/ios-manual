#import <Foundation/Foundation.h>

int main (void)
{
    
        
        


    
 #pragma mark - Lesson 3 properties (Part 1)
/*
****************************************************************************************
 Lesson3 - properies (Part 1)
 
 _name - называются переменные iVar
 
 @synthesize name = _name; - используется команда в имплементации, для того чтоб можно было использовать и сетер и гетер для этого свойства (name). Опеределяет что name в iVar называется _name.
*/
 #pragma mark - Lesson 4 proporties (Part 2)
 /*
 ****************************************************************************************
 Lesson 4 - proporties (Part 2)
 
 @class ASObject; - в хедере файла любого класса, сообщает этому классу, о том что существует такой класс ASObject. Грубо говоря просит поверить наслово.
 
 Параметры свойств:
 - atomic - создается по умолчанию - означает что оно потоконезависимо. (она медленне, доступ к ней медленнее).
 - nonatomic - означает что потокоНЕзащищены. Делается в одном потоке. (она быстрее).
 - readwrite - по умолчанию - доступ к гетерам и сетерам открыт.
 - readonly - доступ к сетерам закрыт.
 - strong - сильная ссылка, если она будет указывать на какой нибудь объект, то объект не может быть уничтожен, пока существует хотя бы одна такая ссылка.
 - weak - ссылка, не влияет на время жизни объекта, тоесть если она указывает на объект, объект тоже будет жить, пока существует хотя бы одна ссылка СТРОНГ на него.
 - assign - тоже самое что weak,но если обект уничтожен, то проперти weak автоматически установится в nil
 - cope:
             {
             в названии класса предка нужно добавить <NSCopying> и создать в хедер файле метод:
                         - (id) copyWithZone:(NSZone *)zone{
                            return [[ASObject alloc] init];
                         }
             В случае если указывается в параметрах свойства copy, то в случае если свойство начинает указывать на новый обеъет, то создается копия объекта и strong ссылка теперь указывает на копию объекта,  а на старый объект не указывает, тоесть объект удаляется.
 
            Недостаток - более медленее.
             }
 - "getter = getObject" - переустановка названия метода get.
 - "setter = setShmetObj" - переустановка названия метода set.
 ###ВАЖНО
 по умолчанию в свойствах устанавливается (assign, atomic, readwrite)
 */
 //****************************************************************************************

 #pragma mark - Lesson 5 - NSArray
/*
 -----------------------------------------------------------------------------------------------------------------------------
 //NSArray * array = [[NSArray alloc] initWithObjects:@"String 1", @"String 3", @"String 1", nil];
 // NSArray * array = [NSArray arrayWithObjects:@"String 1", @"string 2", @"String 3", nil];
 NSArray * array = @[@"String 1", @"string 2", @"String 3"];

 for (NSString * string in array) //выводит последовательно каждое значение строк в массиве array
{
    NSLog(@"%@", string);
    NSLog(@"index = %lu", [array indexOfObject:string]);  //выводит индекс текущего записанной строки в string (при повторении
                                                            строки выводит первый попавшийся индекс)
}
 -----------------------------------------------------------------------------------------------------------------------------
     ASObject * obj1 = [[ASObject alloc] init];
     ASObject * obj2 = [[ASObject alloc] init];
     ASChild * obj3 = [[ASChild  alloc] init];
 
     obj1.name = @"Object 1";
     obj2.name = @"Object 2";
     [obj3 setName:@"Object 3"];
     obj3.lastName  = @"Last name";
 
     NSArray * array = [NSArray arrayWithObjects:obj1, obj2, obj3, nil];
 
     for (ASObject * obj in array) {                //вывод всех данных
         NSLog(@"name = %@", obj.name);
         [obj action];
 
         if([obj isKindOfClass:[ASChild class]]){    // приведения класса ASObject к ASChild
             ASChild * child = (ASChild*) obj;
             NSLog(@"last name = %@", child.lastName);
     }
 
     }
 
 
 */
#pragma mark sortedInArray
/*
students = [students sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
    if ([[obj1 lastName] isEqualToString:[obj2 lastName]])  {
        return [[obj1 name] compare: [obj2 name]];
    }
    else
    {
        return [[obj1 lastName] compare: [obj2 lastName]];
        
    }
}];
 */

 //****************************************************************************************
  #pragma mark - Lesson 6 - Data Types (типы данных)
/*
 CGFloat floatVar = 1.5f; - переменна типа float
 
 NSInteger - число int
 
 NSUInteger - число unsigned int
 
 ###
 СТРУКТУРЫ:
 
 CGPoint point; // структура точки на координатной плоскости
 point.x = 90;
 point.y = 90;
 
 point = CGPointMake(6, 3);
 
 CGSize size;   // структура ширины и высоты прямоугольника
 size.width = 10;
 size.height = 5;
 
 CGRect rect;
 rect.origin = point;
 rect.size = size;
 
 rect = CGRectMake(0, 0, 10, 10);           //совмещает всебе точку на координатной плоскости (начало прямоугольника) и высота и ширина прямоугольника на координатной плоскости
 
 BOOL result =  CGRectContainsPoint(rect, point);
 NSLog(@"%i", result);
 
 ####
 Запись структур в массив
 
 CGPoint point1 = CGPointMake(0,5);
 CGPoint point2 = CGPointMake(3,4);
 CGPoint point3 = CGPointMake(2,8);
 CGPoint point4 = CGPointMake(7,1);
 CGPoint point5 = CGPointMake(4,4);
 
 
 NSArray * array = [NSArray arrayWithObjects:
 [NSValue valueWithCGPoint:point1],                 //перемещает структуру в обьект NSValue
 [NSValue valueWithCGPoint:point2],
 [NSValue valueWithCGPoint:point3],
 [NSValue valueWithCGPoint:point4],
 [NSValue valueWithCGPoint:point5],
 nil];
 
 for (NSValue * value in array) {
 CGPoint p = [value CGPointValue];                      //возращает CGPoint тип данных
 NSLog(@"Point = %@", NSStringFromCGPoint(p));          //распечатка CGPoint через метод NSString
 }
 
 ####
 Запись в массив элементарных данных, используя NSNumber
 
 BOOL boolVar = YES;
 NSInteger intVar = 10;
 NSUInteger uIntVar = 100;
 CGFloat floatVar = 5.5f;
 double doubleVar = 2.5f;
 
 NSNumber * boolObject = [NSNumber numberWithBool:boolVar];
 NSNumber * intObject = [NSNumber numberWithInteger:intVar];
 NSNumber * uIntObject = [NSNumber numberWithUnsignedInteger:uIntVar];
 NSNumber * floatObject = [NSNumber numberWithFloat:floatVar];
 NSNumber * doubleObject = [NSNumber numberWithDouble:doubleVar];
 
 NSArray * array = [NSArray arrayWithObjects:boolObject, intObject, uIntObject, floatObject, doubleObject, nil];
 
 NSLog(@"bool - %i, int - %i, unsigned int - %lu, float - %f, double - %f",
 [[array objectAtIndex:0] boolValue],
 [[array objectAtIndex:1] intValue],
 [[array objectAtIndex:2] unsignedIntegerValue],
 [[array objectAtIndex:3] floatValue],
 [[array objectAtIndex:4] doubleValue]
 );
 */
 
 //****************************************************************************************
  #pragma mark  
 /*
 @protocol ASPatient <NSObject> //NSObject - в данном случае протокол (название протокола) - тоесть наследуется протокол. Возможно добавлять разные протоколы в наследование.
 
 Required (требуемый) - обязятельные требования
 Optional - рекомендованные (опциональные)
 
 
 Сначала надо подключить протокол к хедер файлу.
 <ASPatient> (название протокола - указывается в interface класса возле названия класса наследника.
 
 [obj isKindOfClass:[ASChild class]] - выясняет является ли obj классом ASChild или любого класса наследника.
 - (BOOL)isKindOfClass:(Class)aClass;
 
 
 if ([patient respondsToSelector:@selector (howIsYouJob)]){ // respondsToSelector - наследуется от протокола NSObject. ПРоверяет
                                                            //есть в обьекте patient метод howIsYouJob
    NSLog(@"How is your job? \n%@", [patient howIsYouJob]);
 }
 
 
 if ([patient conformsToProtocol:@protocol(ASPatient)]){ //проверка соответсвует ли обьект протоколу ASPatient
  */

// ****************************************************************************************
  #pragma mark - Lesson 8 - NCDictionary
 /*
 Массив состоящий из ключей ("Key"), каждый который соответсвует определенному указателю на обьект в массиве.
 
 первый вариант создания:
 NSDictionary * dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Petrov", @"lastName", @"Vasiliy", @"name", nil];
 
 второй вариант создания:
 NSDictionary * dictionary3 = @{ @"lastName":@"Petrov", @"name":@"Vasiliy", @"age": @25};
 
 for (NSString * key in [dictionary3 allKeys]){  //вывод массива через цикл
     id obj = [dictionary3 objectForKey:key];
     NSLog(@"\"%@\" - \"%@\"",key, obj);
 }
 
 
 for (id obj in [dictionary3 allValues]){           //вывод только строковых значений в массиве
     if ([obj isKindOfClass:[NSString class]])
     NSLog(@"%@", obj);
 }
 */

 //****************************************************************************************
 #pragma mark - Lesson 9 - Delegates
 /*
 В хедере, при создании проперти - делегата у казывать параметры (weak, nonatomic)
 
 
 ШАБЛОН ДОБАВЛЕНИЯ ДЕЛЕГАТА В КЛАСС:
 ---------------------------------------------------------------
 #import <Foundation/Foundation.h>
 @protocol ASPatientDelegate;
 
 
 @interface ASPatient : NSObject
 
 @property (nonatomic, weak) id <ASPatientDelegate> delegate;
 
 @end
 
 
 @protocol ASPatientDelegate <NSObject>
 
 @required
 - (void) patientFeelsBad: (ASPatient *) patient;
 - (void)patient: (ASPatient *) patient hasQuestion: (NSString *) question;
 
 @end
---------------------------------------------------------------
  */
 
// ****************************************************************************************
 #pragma mark - Lesson 10 - Notifications
 /*
 NSNotificationsCenter - "радио центр" - оповещает о событих классам, которые на него подписаны.
 
В хедере класса, откуда надо получить сообщения объявляются глобальные переменные перед interface
extern NSString* const ASGovernmentTaxLevelDidChangeNotification;
, а в m-file перед implementation его значение
NSString* const ASGovernmentTaxLevelDidChangeNotification = @"ASGovernmentTaxLevelDidChangeNotification";
  
  В сеттере параметра необходимого property:
  создание поста в деволтный нотификацию
  - (void) setTaxLevel:(CGFloat)taxLevel{
        _taxLevel = taxLevel;
        NSDictionary * dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:taxLevel]
                                                                forKey:ASGovernmentTaxLevelUserInfoKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:ASGovernmentTaxLevelDidChangeNotification
                                                            object:nil
                                                          userInfo:dictionary];
      }
  
  В main:
  добавление обсервера:
  [[NSNotificationCenter defaultCenter] addObserver:self                                    //указываешь обсервер (сам на себя)
                                           selector:@selector (governmentNotification:)     //метод в который передается
                                                                                            нотификация(сразу следует его реализовать)
                                               name:ASGovernmentTaxLevelDidChangeNotification //указываем нотификацию которую от
                                                                                                которой ждем изменения
                                             object:nil];
  
  - (void) governmentNotification: (NSNotification *) notification {                //метод обработки нотификации
            NSLog(@"governmentNotification userInfo = %@", notification.userInfo);
  }

  метод очистки нотификации при удалении объекта
  - (void)dealloc
  {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
  }
 */

// ****************************************************************************************
#pragma mark - Lesson 11 - Selectors
/*
 
 @selector - указатель на метод (по факту просто имя метода)
 
 
 // 1 БЛОК
 SEL selector1 = @selector(testMethod);  //создание типа селектора
 SEL selector2 = @selector (testMethod:);
 SEL selector3 = @selector (testMethod:parametr2:);
 
 //1 вариант вызова метода testMethod
 [self performSelector:selector1];       //вызов метода по селектору selector1 (будет выдовать предоупреждение).
 
 
 //2 вариант вызова метода testMethod
 //[self performSelector:@selector(testMethod)];   //не будет выдовать предупреждения
 
 //3 вариант (вызов с параметрами)
 /
 //[self performSelector:selector2 withObject:@"test string"]; /в object указываются параметры, которые необходимо передать. Нельзя передавать обыкновенные типы данных.
 //[self performSelector:selector3 withObject:@"One"  withObject:@"Two"];  //вызов с несколькими параметрами
 
 
 //[self performSelector:selector1 withObject:nil afterDelay:5.f]; //вывод метода с задержкой в 5 сек
 

//2 БЛОК
//использоуется - чтоб убрать ворнинги у селекторов
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"


ASObject  * obj = [[ASObject alloc] init];
SEL selector1 = @selector(testMethod);  //создание типа селектора
[self performSelector:selector1];

[obj performSelector:selector1];

NSString * secret = [obj performSelector:@selector(superSecretTest)]; //распечатка приватного метода superSecretTest с класса Object. Метод приватный, но NSLog он будет выводится. Насильно выводится.
NSLog(@"%@", secret);

[self performSelector:@selector(testMethodParametr1:) withObject:[NSNumber numberWithInt:11] ]; //WRONG. Нельзя передовать примативные типы. Только id.

NSString * str =  NSStringFromSelector(selector1); //создание строки из названия селектора
NSLog(@"Selector in string - %@", str);
SEL sel = NSSelectorFromString(str);
[self performSelector:sel ];                        //создание селектора из строки


//    NSString * string = [self testMethodParametr1:2 parametr2:3.1 parametr3:5.5];
//    NSLog(@"%@", string);

 
// 3 БЛОК (не обязателен)q
 
 
 // ****** вызов селектора с несколькими параметрами (больше чем 2). Не обязательно для узбрежки. Для ознакомления
 SEL selector = @selector(testMethodParametr1:parametr2:parametr3:);
 NSMethodSignature * signature = [AppDelegate instanceMethodSignatureForSelector:selector];
 NSInvocation * inovocation = [NSInvocation invocationWithMethodSignature:signature];
 
 [inovocation setTarget:self];
 [inovocation setSelector:selector];
 
 NSInteger iVal = 2;
 CGFloat fValue = 3.1f;
 double dValue = 5.5f;
 
 NSInteger * p1= &iVal;
 CGFloat * p2 =  &fValue;
 double * p3  = &dValue;
 
 [ inovocation setArgument:p1 atIndex:2];
 [ inovocation setArgument:p2 atIndex:3];
 [ inovocation setArgument:p3 atIndex:4];
 
 [inovocation invoke];  //вызов метода
 
 __unsafe_unretained NSString * string = nil;  // __unsafe_unretained - используется для того, чтобы сказать ARC, что этот объект можно удалить - то что он weak. В другом случае ARC не будет знать что делать и программа упадет (но перед этим выведит string)
 
 [inovocation getReturnValue: &string];
 NSLog(@"%@", string);
 
 
 ##########################################################
 
 при инициализации в ините данная форма запускает метод который указан в селекторе через определенное промежуток временни. В хедере класса нужно обьявить проперти NSTimer* timer;
 
 - (instancetype) initWithName: (NSString *) name andTemperature: (double) temperature{
     self = [super init];
     if (self) {
        _name = name;
        _temperature = temperature;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5                  //промежуток времени
                                                      target:self               //где запускается метод
                                                    selector:@selector (begin)  //название метода
                                                    userInfo:nil
                                                    repeats:YES];
 }
    return self;
 }
 ##########################################################
 
 убрать ворнинги селекторов:
 
 #pragma clang diagnostic push
 #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
 
 [self performSelector:a withObject:b];
 
 #pragma clang diagnostic pop
 
 */

// ****************************************************************************************
#pragma mark - Lesson 12 - Blocks
/*
 
 ПРИ СОЗДАНИИ @property БЛОК - всегда должны быть copy
    @property (copy, nonatomic) вид_блока название_блока;
 
 //возвращаемое_значение название_блока (тип значение передаваемого)
 void (^testBlock) (void); //инициализация блока без принмаего и возращаемого параметра testBlock. равносильно NSObject * testObject;
 
 testBlock = ^ {     //описание методов которые выполняются в блоке
 NSLog(@"test block");
 };
 
 //[self testMethodWithParams:@"Test strinf" valur:111];
 void (^testBlogWithParams) (NSString*, NSInteger) = ^(NSString * string, NSInteger intValue){
 NSLog(@"testMethodWithParams %@ %ld", string, (long)intValue);
 };
 
 testBlogWithParams (@"Test Method", 111);
 
 
 //NSString * result = [self testMethodWithReturnValueAndParams:@"Test String" value:2222];
 //NSLog(@"%@", result);
 NSString * (^testBlogWithReturnValueAndParams) (NSString *, NSInteger) = ^(NSString * string, NSInteger intValue){
 return [NSString stringWithFormat: @"testBlogWithReturnValueAndParams %@ %ld", string, (long)intValue];
 };
 NSLog(@"%@", testBlogWithReturnValueAndParams(@"Hello", 666));
 ############################################################################################
 
 NSString * testString = @"How is it possible?"; //строка будет выводиться в блоке
 
 __block NSString * testStringChange = @"How is it possible?"; //строка будет изменяться в блоке
 
 __block NSInteger i = 0; // __block - указывает на то, что переменная будет изменяться в блоке. по другому не работает
 
 void (^testBlock2) (void);
 
 testBlock2 = ^{
 NSLog(@"test blog");
 NSLog(@"%@", testString);  //преимущество блоков - вывод данных которые созданны за передлами блока. Вот преимущество перед методами. Блок держит строку strong связью. будет существовать, пока существует блок.
 i = i + 1;
 NSLog(@"%li", (long)i);
 
 testStringChange = [NSString stringWithFormat: @"How is it possible?? %li", (long)i]; //вывод строки которая будет каждый раз меняться
 NSLog(@"%@", testStringChange);
 };
 
 testBlock2();
 testBlock2();
 testBlock2();
 testBlock2();
 ############################################################################################
 // использование блоков в массивах для сортировки
 [ self testBlocksMethod:^{
    NSLog(@"Hello, i am a block =) ");
 }];
 NSArray * array = nil;
 NSComparisonResult (^bbb) (id   obj1, id   obj2)= ^(id   obj1, id   obj2){ //использование компаратора для сортировки bbb
 return NSOrderedAscending;
 };
 
 array = [array sortedArrayUsingComparator:bbb];
 ############################################################################################
 
 typedef void(^OurTestBlock)(void); //определения нового типа - OurTestBlock (Блок который ничего не принимает, ничего не передает)
 
 пример использования:
 
 OurTestBlock testBlock = ^{     // тип данных и название блока (вместо void (^testBlock) (void) = ^{ NSLog (@"...")};   )
    NSLog(@"Block 2 !!!!!!");
 };
 ############################################################################################
 
 //ОБЪЕКТЫ В БЛОКАХ
 
 @property (copy, nonatomic) OurTestBlock testBlock;

 ASObject * obj = [[ASObject alloc] init];
 obj.name = @"OBJECT";
 OurTestBlock tb = ^ { //обьект удаляется по выходу с метода
 NSLog(@"%@", obj.name);
 };
 */
/*
 ASObject * obj = [[ASObject alloc] init];
 obj.name = @"OBJECT";
 self.testBlock = ^{         //блок делает на все ссылки strong. Обьекты не удаляются! Блок держит их.
 NSLog(@"%@", obj.name);
 };
 self.testBlock();
 */
/*
ASObject * obj = [[ASObject alloc] init];
obj.name = @"OBJECT";

__weak ASObject * weakObj = obj; //создание слабой ссылки на тот же самый объект obj. В таком случае, при выходе с блок weakObj будет уничтожен

self.testBlock = ^{
    NSLog(@"%@", weakObj.name);   // при таком варианте обьект obj удалится, так как в проперти блока используется слабая ссылка __weak на него
};
self.testBlock();
 
 #######################################
 ВАЖНО - в случае, если мы передаем в метод созданного обьекта - блок, то при использование self.blabla (например) в блоке, будет использовать self обьекта в котором создан обьект, а не обьекта который использует метод.
 
 self.name = @"Hello";
 
 ASObject * obj1 = [[ASObject alloc] init];
 obj1.name = @"OBJECT";
 
 [obj1 testMethod:^{
 NSLog(@"%@", self.name);
 }];
 #######################################
ПРИ ТАКОЙ инициализации при создании обьекта - объект не будет уничитожен, так как self держит strong связью и он  не удаляется из за того, что self использует в блоке
 - (instancetype)init
 {
 self = [super init];
 if (self) {
     self.objectBlock = ^{
        NSLog(@"%@", self.name);   // утечка памяти - блок будет деражть self (тоесть объект)
     };
 }
     return self;
 }
 
 ВЫХОД - создание слабой связи - self - weakSelf
 - (instancetype)init
 {
     self = [super init];
     if (self) {
        __weak ASObject * weakSelf = self;   //создание слабого self, чтоб обьъект удалился после того как перестал использваться
     self.objectBlock = ^{
        NSLog(@"%@", weakSelf.name);
     };
     }
     return self;
 }
 
*/


    
// ****************************************************************************************
#pragma mark - Lesson 13 - Multithreading
/*
Main Thread - основной (главный) поток! На основном потоке меняется графика, графические объекты, перерсовка! (Draw UI)
 Можно создать отдельные потоки. Главному потоку придается приоритет - графике.

[self  performSelectorInBackground:@selector(testThread) withObject:nil]; //вызов метода c селектором testThread в Backgrond
 
 Когда создается поток, нужно создавать Autorelease Pool в котором хранятся все обьекты и все что происходит внутри. Если не указать то когда thread закончится, то объекты которые там использдовались не удалятся - произойдет утечка памяти.

        @autoreleasepool {
            //...... код
        }
 ########################################################
 
 //создание новой переменной, которая запускает метод (селектор) в новый поток.
NSThread * thread = [[NSThread alloc] initWithTarget:self //у какого объекта будет вызван
                                            selector:@selector(testThread) // какой селектор будет вызван
                                              object:nil];
[thread start];     // запуск нового потока
 
 [NSThread currentThread] - возвращает текущий поток.
 [[NSThread currentThread] nsme] - возвращает имя текущего потока
 ########################################################
 double startTime = CACurrentMediaTime(); //текущее время
 CACurrentMediaTime() - startTime         //разница между начальным значением времени и последующим (возвращает float)
 
 ########################################################
 [[NSThread currentThread]isMainThread];   // возвращает булевское значение - показывает на главном ли потоке находится.
 
 ########################################################
 
    //ЗАМОК НА ПОТОК:
    @synchronized(self){            //ставит блок на код! Пока не отработает один поток, второй сюда не зайдет. Когда зашел второй,
                                      то он ставит замок и третий не зайде, пока второй не отработает.
        
        NSLog(@" %@ calculations started", [[NSThread currentThread] name]);
        for (int i = 0; i < 20000 ; i++){
            [self.array addObject:string];
        }
        NSLog(@"%@ calculations - finished", [[NSThread currentThread] name]);
    }
 
 ########################################################
 Grand Central Dispatch (GCD) - технология Apple, предназначенная для создания приложений, использующих преимущества многоядерных процессоров и других SMP-систем[1]. Эта технология является реализацией параллелизма задач и основана на шаблоне проектирования «Пул потоков». GCD впервые была представлена в Mac OS X 10.6. Исходные коды библиотеки libdispatch, реализующей сервисы GCD, были выпущены под лицензией Apache 10 сентября 2009 г.[1]. Впоследствии библиотека была портирована[2] на другую операционную систему FreeBSD [3].
 
 ########################################################
 
 Создание синглтона (Singleton) - https://makeomatic.ru/blog/2014/01/13/Singleton/
 “При одновременном вызове из нескольких потоков, эта функция синхронно ждет до тех пор, пока блок выполнится”
 
    + (instanceType *)sharedInstance {
        static instanceType *sharedInstance = nil;
        static dispatch_once_t onceToken; // onceToken = 0
        dispatch_once(&onceToken, ^{
            sharedInstance = [[instanceType alloc] init];
        });
        return sharedInstance;
    }
        
     */
    
    
    // ****************************************************************************************
#pragma mark - Lesson 14 - NSString
    
    // вывод строки до 10 индекса (включительно)
    //    text = [text substringToIndex:10];
    // вывод строки использую Range
    //      text = [text substringWithRange:NSMakeRange(10, 10)];
    
    /*
     // поиск индекса расположения строки "All lengths"
     NSRange range = [text rangeOfString:@"All lengths"];
     // соркращение строки до строки @"All lengths"
     if (range.location != NSNotFound)
     {
     text = [text substringToIndex:range.location];
     }
     */
    
    
    // количество совпадений с со строкой @"NSString"
    /*
     NSRange searchRange = NSMakeRange(0, [text length]);
     NSInteger counter = 0;
     while (YES) {
     NSRange range = [text rangeOfString:@"NSString" options:0 range:searchRange];
     if (range.location != NSNotFound)
     {
     NSInteger index = range.location + range.length;
     searchRange.location = index;
     searchRange.length = [text length] - index;
     NSLog(@"%@", NSStringFromRange(range));
     counter++;
     }
     else
     break;
     }
     NSLog(@"Counter = %ld", (long)counter);
     */
    
    //замена строки @"NSStrng" на @"TRALALA"
    /*
     text = [text stringByReplacingOccurrencesOfString:@"NSString" withString:@"TRALALA"];
     */
    /*
     //все буквы маленькие в тексте
     text = [text lowercaseString];
     //все буквы заглавные в тексте
     text = [text uppercaseString];
     //сделать каждое слово в тексте с большой буквы
     text = [text capitalizedString];
     */
    // перевод строки в int
    /*
     NSLog(@"%d", [@"512" intValue]);
     */
    /*
     // удаление в тексте , . ( )
     text = [text stringByReplacingOccurrencesOfString:@"," withString:@""];
     text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
     text = [text stringByReplacingOccurrencesOfString:@"(" withString:@""];
     text = [text stringByReplacingOccurrencesOfString:@")" withString:@""];
     NSCharacterSet * quoteCharset = [NSCharacterSet characterSetWithCharactersInString:@"\""];
     text = [text stringByTrimmingCharactersInSet:quoteCharset];
     
     // создание массива с разбитым текстом на слова - словами (поделенными пробелами)
     NSArray * array = [text componentsSeparatedByString:@" "];
     
     //создание текста с текстовых компонентов массива, разделенными @"_" (андерскор)
     text = [array componentsJoinedByString:@"_"];
     
     NSLog(@"%@", array);
     */
    
    //сложение строк
/*
    NSString * s1 = @"Hello ";
    NSString * s2 = @"World";
    NSString * s3 = [s1 stringByAppendingString:s2];
    NSLog(@"%@", s3);
    
    NSLog(@"%@", text);
*/
    
#pragma mark - Lesson 15 - Битовые операции (Bitwise operations )
/*
    //как узнать установлен ли бит в маске или нет - нужно побитово умножить маску на этот бит
    11100011 & 00010000 = 00000000 = NO
    11100011 & 00100000 = 00100000 = YES
    
    //Как сбросить бит в маске (2 действия)
    1) Инвертируем нужны бит:  ~00000010 = 11111101
    2) Производим битовое умножение с маской: 01100011 & 11111101 = 01100001

// ****************************************************************************************
 
    // создание энума - маски с флагами
    typedef enum {
        ASStudentSubjectTypeBiology =       1 << 0, // = 00000001
        ASStudentSubjectTypeMath =          1 << 1, // = 00000010
        ASStudentSubjectTypeDevelopment =   1 << 2, // = 00000100
        ASStudentSubjectTypeEngineering =   1 << 3, // = 00001000
        ASStudentSubjectTypeArt =           1 << 4, // = 00010000
        ASStudentSubjectTypePhycology =     1 << 5, // = 00100000
        ASStudentSubjectTypeAnatomy =       1 << 6  // = 01000000
    } ASStudentSubjectType;
    
    // создание property в классе для хранения флагов:
@property(assign, nonatomic) ASStudentSubjectType subjectType;
    
    
    
- (NSString *)description
{
    // не коректная версия вывода дискрипшна по битово (без self метода)
//     return [NSString stringWithFormat:@"Student studies:\n"
//     "biology = %@\n"
//     "math = %@\n"
//     "development = %@\n"
//     "engineering = %@\n"
//     "art = %@\n"
//     "phycology = %@\n"
//     "anatomy = %@\n",
//     self.subjectType & ASStudentSubjectTypeBiology ? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypeMath ? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypeDevelopment ? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypeEngineering? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypeArt? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypePhycology ? @"YES" : @"NO",
//     self.subjectType & ASStudentSubjectTypeAnatomy ? @"YES" : @"NO"];
    
    // КОРЕКТНАЯ версия вывода дискрипшна - через self метод
    return [NSString stringWithFormat:@"Student studies:\n"
            "biology = %@\n"
            "math = %@\n"
            "development = %@\n"
            "engineering = %@\n"
            "art = %@\n"
            "phycology = %@\n"
            "anatomy = %@\n",
            [self answerByType:ASStudentSubjectTypeBiology],
            [self answerByType:ASStudentSubjectTypeMath],
            [self answerByType:ASStudentSubjectTypeDevelopment],
            [self answerByType:ASStudentSubjectTypeEngineering],
            [self answerByType:ASStudentSubjectTypeArt],
            [self answerByType:ASStudentSubjectTypePhycology],
            [self answerByType:ASStudentSubjectTypeAnatomy] ];
    
}

// удаления нужных флагов в проперти:
- (void)removeSubjectType:(ASStudentSubjectType)subject
{
    self.subjectType = (~subject) & self.subjectType;
}

// метод для использования в дискрипшине (вывод YES или NO)
- (NSString *) answerByType: (ASStudentSubjectType) subject
{
    return self.subjectType & subject ? @"YES" : @"NO";
}
    
@end
*/
    //
    
    // ****************************************************************************************



#pragma mark - Lesson 17 - UIViewController
    
/*
     
     - (void) loadView
     {
     [super loadView];
     NSLog(@"LoadView");
     }
     
     - (void)viewDidLoad {
         [super viewDidLoad];
         //self.view.backgroundColor = [UIColor redColor]; //изменение цвета фона
 
         if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)        // УЗНАТЬ КАКОЙ ГАДЖЕТ - iPhone or iPad
             self.view.backgroundColor = [UIColor redColor];
         else
             self.view.backgroundColor = [UIColor blueColor];
 
 
         NSLog(@"viewDidLoad");
     }
     
     #pragma mark - Views
     
     - (void)viewWillAppear:(BOOL) animated
     {
         [super viewWillAppear:animated];
         NSLog(@"viewWillAppear");
     }
     
     - (void)viewDidAppear:(BOOL)animated
     {
         [super viewDidAppear:animated];
         NSLog(@"viewDidAppear");
     }
     - (void)viewWillDisappear:(BOOL)animated
     {
         [super viewWillDisappear:animated];
         NSLog(@"viewWillDisappear");
     }
     - (void)viewDidDisappear:(BOOL)animated
     {
         [super viewDidDisappear:animated];
         NSLog(@"viewDidDisappear");
     }
     
     - (void) viewWillLayoutSubviews
     {
         [super viewWillLayoutSubviews];
         NSLog(@"viewWillLayoutSubviews");
     }
     
     - (void) viewDidLayoutSubviews
     {
         [super viewDidLayoutSubviews];
         NSLog(@"viewDidLayoutSubviews");
     }
     
     #pragma mark Orientation     ориентация изображения телефона
     
     
     // при возвращение NO - приложение не переварачивается
         - (BOOL)shouldAutorotate
     {
     return YES;
     }
     
     // выбор в маске в какое ориентации работает приложение
         - (UIInterfaceOrientationMask)supportedInterfaceOrientations
     {
     // приложение только Home слева и Home с права
         //return  UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
     
     // приложение только Home слева и Home с права, и портретное
         //return  UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortrait;
     
     // приложение только поддерижвает все повороты
         return  UIInterfaceOrientationMaskAll;
     }
     
     
     // предпочительная ориентация, если запущен как модальный
     - (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
     {
     
     }
     
     
     
     // телефон (изображение) будет сейчас перевернут в положение toInterfaceOrientation
     - (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
     {
     NSLog(@"willRotateToInterfaceOrientation from %ld to %ld", [[UIApplication sharedApplication] statusBarOrientation], (long)toInterfaceOrientation);
     }
     // телефон уже перевернулся из положение fromInterfaceOrientation
     - (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
     {
        NSLog(@"didRotateFromInterfaceOrientation %ld", (long)fromInterfaceOrientation);
     }
     
*/
    
    
// ****************************************************************************************
#pragma mark - Lesson 19 - UIView Geometry
/*
    - (void)viewDidLoad {
        [super viewDidLoad];
        UIView * view1 = [[UIView alloc] initWithFrame:CGRectMake(100, 150, 200, 50)]; // 100 X 100 - координаты, 200 Х 50 - размеры view
        view1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
        // добавления на основной view только что созданную новую view
        [self.view addSubview:view1];
        
        UIView * view2 = [[UIView alloc] initWithFrame:CGRectMake(80, 130, 50, 250)];
        view2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
        //изменение поведения view при перевороте телефона (flexible - придание объекту гибкости)
        view2.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        self.testView = view2;
        
        // добавления на основной view только что созданную новую view
        [self.view addSubview:view2];
        
        //  view1.superview  -  возвращает view на котором находится view1
        //  view1.subviews   -  возвращает массив view на которых находится view1
        
        //вынести на передний план view1
        [self.view bringSubviewToFront:view1];
        
        
        [self supportedInterfaceOrientations];
    }
    
    // метод возвращает маска - поддерживаемые ориентиры телефона
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations
    {
        return UIInterfaceOrientationMaskAll; // поддерживает все перевороты
    }
    
    - (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
    }
    
    - (void)viewWillTransitionToSize:(CGSize)size
withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
    {
        
        // frame - координаты положения subView относительно superView (у главного View - левый верхний угол - 0,0)
        // bounds - точка координат отчитывается от положения subView (левый верхний угол принимается за 0,0)
        // NSLog(@"\nframe = %@\nbounds = %@", NSStringFromCGRect(self.testView.frame), NSStringFromCGRect(self.testView.bounds));
        
        NSLog(@"\nframe = %@\nbounds = %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
        
        
        [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
            NSLog(@"\nOrientation - %li", orientation);
            CGPoint origin = CGPointZero;
            origin = [self.view convertPoint:origin fromView:self.view.window];
            NSLog(@"\norigin = %@", NSStringFromCGPoint(origin));
            
        } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            
        }];
        
        
        CGPoint origin = CGPointZero;
        origin = [self.view convertPoint:origin fromView:self.view.window];
        NSLog(@"origin = %@", NSStringFromCGPoint(origin));
    }
 */

    // ****************************************************************************************
#pragma mark - Lesson 21 - UIViewAnimations
/*
##########################################################################################################################
//- (void)viewDidLoad {
//    [super viewDidLoad];
////
////     UIView *  view = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
////     view.backgroundColor = [UIColor greenColor];
////     [self.view addSubview:view];
////
////     self.testView = view;
//
//
//
//    self.view.backgroundColor = [UIColor greenColor];
//    UIImageView *  view = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
//    view.backgroundColor = [UIColor clearColor];
//    UIImage * image1 = [UIImage imageNamed:@"Man1.png"];
//    UIImage * image2 = [UIImage imageNamed:@"Man2.png"];
//    NSArray * images = [NSArray arrayWithObjects:image1, image2, nil];
//    view.animationImages = images;
//
//    view.animationDuration = 1.f;
//    [view startAnimating];
//
//
//    [self.view addSubview:view];
//    self.testImageView = view;
//}
//##########################################################################################################################
//- (void) viewDidAppear:(BOOL)animated  //переопределение метода (вью появляется)
//{
//    [super viewDidAppear:animated];
//
//    //[self moveView:self.testView];
//    [self moveView:self.testImageView];
    
##########################################################################################################################
//     [UIView animateWithDuration:5
//     animations:^{
//     self.testView.center = CGPointMake(CGRectGetWidth(self.view.bounds) - (CGRectGetWidth(self.testView.frame) / 2), 150);
//     }];
##########################################################################################################################
//     [UIView animateWithDuration:(NSTimeInterval)
//     animations:^{
//
//     }
//     completion:^(BOOL finished) {
//
//     }];
##########################################################################################################################
//     [UIView animateWithDuration:10
//     delay:1
//     //маска! опции анимации  UIViewAnimationOptionBeginFromCurrentState - начало анимации с текущего состояния объекта
//     options: UIViewAnimationOptionCurveEaseInOut //| UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
//     animations:^{
//     self.testView.center = CGPointMake(CGRectGetWidth(self.view.bounds) - (CGRectGetWidth(self.testView.frame) / 2), 150);
//
//     } completion:^(BOOL finished) {
//     NSLog(@"animation finished! %d", finished);
//     }];
//
//     // отключение всех анимаций после 10 секунд
//     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//     //[self.testView.layer removeAllAnimations];
//     [UIView animateWithDuration:4
//     delay:0
//     options: UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
//     animations:^{
//     self.testView.center = CGPointMake(500, 500);
//     } completion:^(BOOL finished) {
//     NSLog(@"animation finished! %d", finished);
//     }];
//     });
##########################################################################################################################

     //изменение цвета в анимации
     [UIView animateWithDuration:10
     delay:1

     options: UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
     animations:^{
     self.testView.center = CGPointMake(CGRectGetWidth(self.view.bounds) - (CGRectGetWidth(self.testView.frame) / 2), 150);
     self.testView.backgroundColor = [self randomColor];
     } completion:^(BOOL finished) {
     NSLog(@"animation finished! %d", finished);
     }];
##########################################################################################################################
//     //изменение transform - матрица преобразования, для двумерного. - Операция поворота, изменение масштаба, перенос
//     [UIView animateWithDuration:10
//     delay:1
//
//     options: UIViewAnimationOptionCurveEaseInOut //| UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
//     animations:^{
//     //                         self.testView.center = CGPointMake(CGRectGetWidth(self.view.bounds) - (CGRectGetWidth(self.testView.frame) / 2), 150);
//     //                         self.testView.backgroundColor = [self randomColor];
//
//     //self.testView.transform = CGAffineTransformMakeRotation(M_PI);  //аргумент - выбираем угол. Поворот на pi
//     //self.testView.transform = CGAffineTransformMakeTranslation(600, 0); //перемещение по x или по y
//     //self.testView.transform = CGAffineTransformMakeScale(2, 0.5);  //изменение размера по x в 2 раза, по y в 0.5 раз
//
//     // несколько действий transform в одном
//     CGAffineTransform scale = CGAffineTransformMakeScale(2, 0.5);
//     CGAffineTransform rotation = CGAffineTransformMakeRotation(M_PI );
//     CGAffineTransform transform = CGAffineTransformConcat(scale, rotation); //сложение несколько матриц в одну
//     self.testView.transform = transform;
//
//     } completion:^(BOOL finished) {
//     NSLog(@"animation finished! %d", finished);
//
//     NSLog(@"\ntestVief frame = %@\ntestView bounce = %@", NSStringFromCGRect(self.testView.frame), NSStringFromCGRect(self.testView.bounds));
//     }];
##########################################################################################################################
    
//
//}
//
//- (void) moveView: (UIView *) view
//{
//
//    CGRect rect = self.view.bounds;
//
//    rect = CGRectInset(rect, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
//
//    CGFloat x = arc4random() % (int)CGRectGetWidth(rect) + CGRectGetMinX(rect);
//    CGFloat y = arc4random() % (int)CGRectGetHeight(rect) + CGRectGetMinY(rect);
//
//    CGFloat s = (float)(arc4random() % 151) / 100.f + 0.5f;
//
//    CGFloat r = (float)(arc4random() % (int)(M_PI * 2 * 10000)) / 10000 - M_PI;
//
//    CGFloat d  =(float) (arc4random() % 20001) / 10000 + 2;
//
//    [UIView animateWithDuration:d
//                          delay:0
//
//                        options: UIViewAnimationOptionCurveLinear
//                     animations:^{
//                         view.center = CGPointMake(x, y);
//                         view.backgroundColor = [self randomColor];
//
//
//
//
//                         CGAffineTransform scale = CGAffineTransformMakeScale(s, s);
//                         CGAffineTransform rotation = CGAffineTransformMakeRotation(r);
//
//
//                         CGAffineTransform transform = CGAffineTransformConcat(scale, rotation);
//
//                         view.transform = transform;
//
//                     } completion:^(BOOL finished) {
//                         NSLog(@"animation finished! %d", finished);
//                         NSLog(@"\ntestVief frame = %@\ntestView bounce = %@", NSStringFromCGRect(self.testView.frame), NSStringFromCGRect(self.testView.bounds));
//                         NSLog(@"rect - %@", NSStringFromCGRect(rect));
//
//                         __weak UIView * v = view;
//                         [self moveView:v];
//
//                     }];
//}

##########################################################################################################################
//- (UIColor *) randomColor
//{
//    CGFloat r = (CGFloat)(arc4random() % 256) / 255;
//    CGFloat g = (CGFloat)(arc4random() % 256) / 255;
//    CGFloat b = (CGFloat)(arc4random() % 256) / 255;
//
//    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
//}
*/
 
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 22 - UITouches
/*
#import "ViewController.h"
    
    @interface ViewController ()
    
    //@property (weak,nonatomic) UIView * testView;
    @property (weak,nonatomic) UIView * draggingView;
    @property (assign, nonatomic) CGPoint touchOfSet;
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        for (NSInteger i =0; i < 8; i++) {
            UIView * view = [[UIView alloc] initWithFrame:CGRectMake(10 + 110 * i, 100, 100, 100)];
            view.backgroundColor = [self randomColor];
            [self.view addSubview:view];
        }
        //    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
        //    view.backgroundColor = [UIColor greenColor];
        //    [self.view addSubview:view];
        
        //self.testView = view;
        
        //self.view.multipleTouchEnabled = YES;                                           // разрешает одновременное касание нескольками пальцами
        
    }
    
    - (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
    }
    
    - (void) logTouches: (NSSet *) touches WithMethod: (NSString *) methodName
    {
        NSMutableString * string = [NSMutableString stringWithString:methodName];
        for (UITouch * touch in touches) {
            CGPoint point = [touch locationInView:self.view];                   //показывает текущее положение прикосновения в заданном view
            [string appendFormat:@" %@", NSStringFromCGPoint(point)];
        }
        NSLog(@"%@", string);
    }
    
    - (UIColor *) randomColor
    {
        CGFloat r = (CGFloat)(arc4random() % 256) / 255;
        CGFloat g = (CGFloat)(arc4random() % 256) / 255;
        CGFloat b = (CGFloat)(arc4random() % 256) / 255;
        
        return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
    }
    
#pragma mark Touches
    
    - (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
    {
        [self logTouches:touches WithMethod:@"touchesBegan"];
        
        //    UITouch * touch = [touches anyObject];                      //взять любой обьект с сет
        //    CGPoint point = [touch locationInView: self.testView];      //отслеживание тача на testView
        //    NSLog(@"inside = %d", [self.testView pointInside:point withEvent: event]);              //проверка - нажали мы на self.testView
        
        UITouch * touch = [touches anyObject];
        CGPoint pointOnMainView = [touch locationInView: self.view];
        UIView * view  = [self.view hitTest: pointOnMainView withEvent: event];         //возвращает вью, которя лежит самая врехняя на self.view. Если ничего нет, то он возвращает собственно self.view
        if (![view isEqual:self.view])
        {
            self.draggingView = view;
            [self.view bringSubviewToFront:view];
            CGPoint touchPoint = [touch locationInView:self.draggingView];
            self.touchOfSet =  CGPointMake(CGRectGetMidX(self.draggingView.bounds) - touchPoint.x,
                                           CGRectGetMidY(self.draggingView.bounds) - touchPoint.y);
            //[self.draggingView.layer removeAllAnimations];     //отмена всех анимация
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.draggingView.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
                                 self.draggingView.alpha = 0.3;
                             }];
            
        } else
        {
            self.draggingView = nil;
        }
        
    }
    - (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
    {
        [self logTouches:touches WithMethod:@"touchesMoved"];
        
        if (self.draggingView)
        {
            UITouch * touch = [touches anyObject];
            CGPoint pointOnMainView = [touch locationInView: self.view];
            
            CGPoint correction = CGPointMake(pointOnMainView.x + self.touchOfSet.x,
                                             pointOnMainView.y + self.touchOfSet.y);
            self.draggingView.center = correction;
        }
        
    }
    - (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
    {
        [self logTouches:touches WithMethod:@"touchesEnded"];
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.draggingView.transform = CGAffineTransformIdentity;           //поставить все transform по умолчанию
                             self.draggingView.alpha = 1.f;
                         }];
        self.draggingView = nil;
    }
    - (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
    {
        [self logTouches:touches WithMethod:@"touchesCancelled"];
        
        
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.draggingView.transform = CGAffineTransformIdentity;           //поставить все transform по умолчанию
                             self.draggingView.alpha = 1.f;
                         }];
        self.draggingView = nil;
    }
    
    @end
 
 // игнорирование тачей на определенное время
 [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
 
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
 if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
 });
*/
    
 
// ***********************************************************************************************************************
#pragma mark - Lesson 23 - UIGestureRecognizer
    
/*
#import "ViewController.h"
    
    @interface ViewController () <UIGestureRecognizerDelegate>  // подписка класса на протокол, который используется для одновременного использования двух gesture (pinch and rotation например)
    
    @property (weak, nonatomic) UIView * testView;
    
    @property (assign, nonatomic) CGFloat testViewScale;
    @property (assign, nonatomic) CGFloat testViewRotation;
    @property (assign, nonatomic) CGPoint testViewDeltaOfPan;
    @property (assign, nonatomic) BOOL beganMoved;
    
    @end
    
    @implementation ViewController
    
    
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        self.testViewScale = 0.f;
        
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 50,
                                                                 CGRectGetMidY(self.view.bounds) - 50,
                                                                 100,
                                                                 100)];
        view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin; //установка авторесайза, чтоб не менялась ширина, высота
        view.backgroundColor = [UIColor greenColor];
        [self.view addSubview:view];
        self.testView = view;
        
        
        
        
        //создание одинарного тапа тапа
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector (handleTap:)];
        // tapGesture.numberOfTapsRequired = 2;   // теперь будет отклик на двойной тап только
        // tapGesture.numberOfTouchesRequired = 2;  //+отклик на одновременно два касания (тапа)
        
        //@property (nonatomic) NSUInteger  numberOfTapsRequired;    //свойства  UITapGestureRecognizer - количество тапов (щелканей) по экрану
        //@property (nonatomic) NSUInteger  numberOfTouchesRequired __TVOS_PROHIBITED;    // сколько одновременно пальцев касаются
        //    UILongPressGestureRecognizer - обьект класса в ктором выставляется время нажатия на экран
        //создание двойного тапа
        UITapGestureRecognizer * doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector (handleDoubleTap:)];
        doubleTapGesture.numberOfTapsRequired = 2;
        [self.view addGestureRecognizer:tapGesture];    //добавление на наше вью recognizer
        [self.view addGestureRecognizer:doubleTapGesture];
        
        
        [tapGesture requireGestureRecognizerToFail:doubleTapGesture];   //подождет немного не сработает ли doubleTapGesture, а потом если не сработает, то он работает (таким методом можно назначить только один регонайзер
        
        
        //создание двойного тапа с двумя тачами
        UITapGestureRecognizer * doubleTapDoubleTouchGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                       action:@selector (handleDoubleTapDoubleTouch:)];
        doubleTapDoubleTouchGesture.numberOfTapsRequired = 2;
        doubleTapDoubleTouchGesture.numberOfTouchesRequired = 2;
        [self.view addGestureRecognizer:doubleTapDoubleTouchGesture];
        
        //>>>>>>>>>>>>>>>>>>>>>>>>> ZOOM. Увелечение уменьшение (зумирование) - UIPinchGestureRecognizer
        
        UIPinchGestureRecognizer * pinchGesture =  [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                             action:@selector(handlePinch:)];
        pinchGesture.delegate = self;  //добавление делегата, которое используется для того чтоб разрешить одновременную работу с rotation (метод делегата ниже)
        [self.view addGestureRecognizer: pinchGesture];
        
        // <<<<<<<<<<<<<<<<<<<<<<<<<
        
        //>>>>>>>>>>>>>>>>>>>>>>>>> осуществления повоторати вью UIRotationGestureRecognizer
        
        UIRotationGestureRecognizer * rotationGesture =  [[UIRotationGestureRecognizer alloc] initWithTarget:self
                                                                                                      action:@selector(handleRotation:)];
        rotationGesture.delegate = self;  //добавление делегата, которое используется для того чтоб разрешить одновременную работу с pinch (метод делегата ниже)
        [self.view addGestureRecognizer: rotationGesture];
        // <<<<<<<<<<<<<<<<<<<<<<<<<
        
        //>>>>>>>>>>>>>>>>>>>>>>>>> Pan - перемещение
        UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(handlePan:)];
        [self.view addGestureRecognizer: panGesture ];
        // <<<<<<<<<<<<<<<<<<<<<<<<<
        
        //>>>>>>>>>>>>>>>>>>>>>>>>> cвап Swipe
        //      у swipe есть свойство direction - это маска. -
        
        //    typedef NS_OPTIONS(NSUInteger, UISwipeGestureRecognizerDirection) {
        //        UISwipeGestureRecognizerDirectionRight = 1 << 0,
        //        UISwipeGestureRecognizerDirectionLeft  = 1 << 1,
        //        UISwipeGestureRecognizerDirectionUp    = 1 << 2,
        //        UISwipeGestureRecognizerDirectionDown  = 1 << 3
        //    };
        UISwipeGestureRecognizer * verticalSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                    action:@selector(handleSwipeVertical:)];
        verticalSwipeGesture.direction = UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: verticalSwipeGesture ];
        
        
        UISwipeGestureRecognizer * gorizontalSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                                      action:@selector(handleSwipeGorizontal:)];
        gorizontalSwipeGesture.direction =UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer: gorizontalSwipeGesture ];
        
        // <<<<<<<<<<<<<<<<<<<<<<<<<
    }
    
#pragma mark gestures
    
    - (void) handleSwipeGorizontal: (UISwipeGestureRecognizer *) gestureGorizontal
    {
        NSLog(@"Swipe Gorizontal");
    }
    
    - (void) handleSwipeVertical: (UISwipeGestureRecognizer *) gestureVertical
    {
        NSLog(@"Swipe Vertical");
        self.testView.center = CGPointMake(self.testView.center.x, self.testView.center.y + 20);
    }
    
    - (void) handlePan: (UIPanGestureRecognizer *) gesture
    {
        
        // >>>>>>>>>>>>>>>>>>> смена положения view при перетаскивании
        
        if (gesture.state == UIGestureRecognizerStateEnded)
            self.beganMoved = NO;
        
        if ( CGRectContainsPoint(self.testView.frame, [gesture locationInView:self.view]) && gesture.state == UIGestureRecognizerStateBegan )
        {
            self.beganMoved = YES;
            CGFloat deltaX = [gesture locationInView:self.view].x - CGRectGetMidX(self.testView.frame);
            CGFloat deltaY = [gesture locationInView:self.view].y - CGRectGetMidY(self.testView.frame);
            self.testViewDeltaOfPan = CGPointMake(deltaX, deltaY);
        }
        
        if (self.beganMoved)
            self.testView.center = CGPointMake([gesture locationInView:self.view].x - self.testViewDeltaOfPan.x,
                                               [gesture locationInView:self.view].y - self.testViewDeltaOfPan.y);
        
        // <<<<<<<<<<<<<<<<<<<
    }
    
    - (void) handleRotation: (UIRotationGestureRecognizer *) gesture
    {
        NSLog(@"Handle Rotation %1.3f", gesture.rotation);
        
        
        // >>>>>>>>>>>>>>>>>>>>>>> вращение вью
        if (gesture.state == UIGestureRecognizerStateBegan)  //в случае начала действия пинча
            self.testViewRotation = 0.f;  //установка в ноль для того, чтоб при начале кручения не было рывка
        
        CGFloat newRotation = gesture.rotation - self.testViewRotation;
        
        CGAffineTransform currentTransform = self.testView.transform;
        CGAffineTransform newTransform = CGAffineTransformRotate (currentTransform, newRotation);
        
        self.testView.transform = newTransform;
        
        self.testViewRotation = gesture.rotation;
        
        // <<<<<<<<<<<<<<<<<<<<<<
    }
    
    - (void) handlePinch: (UIPinchGestureRecognizer *) gesture
    {
        NSLog(@"Handle Pinch: %1.3f", gesture.scale); //расстояние между пальцами
        
        // >>>>>>>>>>>>>>>>>>> увелечения вью при pinch gesture
        
        if (gesture.state == UIGestureRecognizerStateBegan)  //в случае начала действия пинча
            self.testViewScale = 1.f;
        
        CGFloat deltaScale = gesture.scale - self.testViewScale + 1.f;
        
        CGAffineTransform currentTransform = self.testView.transform;
        CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, deltaScale, deltaScale);
        self.testViewScale = gesture.scale;
        
        self.testView.transform = newTransform;
        // <<<<<<<<<<<<<<<<<<
        
    }
    
    - (void) handleTap: (UITapGestureRecognizer *) tapGesture
    {
        NSLog(@"Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
        
        if ( CGRectContainsPoint(self.testView.frame, [tapGesture locationInView :self.view]) )
            self.testView.backgroundColor = [self randomColor];
    }
    
    - (void) handleDoubleTap: (UITapGestureRecognizer *) tapGesture
    {
        NSLog(@"Double Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
        
        // >>>>>>>>>>>>>>>>>>> увелечения scale при каждом двойном тапе
        CGAffineTransform currentTransform = self.testView.transform;
        CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 1.2f, 1.2f);
        
        if ( CGRectContainsPoint(self.testView.frame, [tapGesture locationInView :self.view]) )
            [UIView animateWithDuration:0.3
                             animations:^{
                                 
                                 self.testView.transform = newTransform;
                             }];
        self.testViewScale = 1.2f;
        
        
        // <<<<<<<<<<<<<<<<<<
    }
    
    - (void) handleDoubleTapDoubleTouch: (UITapGestureRecognizer *) tapGesture
    {
        NSLog(@"Double Touch Double Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
        
        // >>>>>>>>>>>>>>>>>>> уменьшение scale при каждом двойном тапе
        CGAffineTransform currentTransform = self.testView.transform;
        CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 0.8f, 0.8f);
        
        if ( CGRectContainsPoint(self.testView.frame, [tapGesture locationInView :self.view]) )
            [UIView animateWithDuration:0.3
                             animations:^{
                                 
                                 self.testView.transform = newTransform;
                             }];
        self.testViewScale = 1.2f;
        // <<<<<<<<<<<<<<<<<<
        
    }
    
#pragma mark - UIGestureRecognizerDelegate - протокол UIGestureRecognizerDelegate
    
    
    //>>>>>>>>>>>>>>>>>>>>>>> метод содержится в протоколе делегатах UIGestureRecognizer и используется для того, чтоб определить два gesture которые будут срабатывать одновременно - армгументы которые передатутся будут срабатывать вместе если вернуть YES
    - (BOOL)                                 gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
    {
        return YES;
    }
    // <<<<<<<<<<<<<<<<<<<<<<<
    
#pragma mark - Methods
    
    - (UIColor *) randomColor
    {
        CGFloat r = (CGFloat)(arc4random() % 256) / 255;
        CGFloat g = (CGFloat)(arc4random() % 256) / 255;
        CGFloat b = (CGFloat)(arc4random() % 256) / 255;
        
        return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
    }
    
    
    @end
    
*/
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 24 - UIDrawing
    
    //Коментарии
    
    //!!!!!!!!!! - ссылка на хабрахабр https://habrahabr.ru/post/117824/
    
    //[self.drawingView setNeedsDisplay]; //метод говорит, что вью должна быть перерисована. Вызывается в ViewControler.m - при изменении ориентации, чтоб не рстягивалась данная вью
    /*
#import "ASDrawingView.h"
    
    @implementation ASDrawingView
    
    - (void)drawRect:(CGRect)rect
    { //можно перерисовывать элементы при его вырисовки
        [super drawRect:rect];
        
        
         NSLog(@"drawRect %@", NSStringFromCGRect(rect));
         
         //сишные функции используются для рисования
         CGContextRef context = UIGraphicsGetCurrentContext();  //Взятия контекста нашей вью. Ref - в конце функции означает,что это структура которая переопределна с указателя
         //CGContextSetStrokeColor(<#CGContextRef  _Nullable c#>, <#const CGFloat * _Nullable components#>) // вырисовка цвета границ
         //CGContextSetFillColor(<#CGContextRef  _Nullable c#>, <#const CGFloat * _Nullable components#>) // вырисока бэкграунд цвета
         
         // >>>>>>>>>>>>>>>>> изменение цвета вью в rect площаде (1 спосооб)
         //CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor); // назначение цвет бэкграунда красным
         //CGContextFillRect(context, rect);                             //вырисовка красного бэкграунда на площаде rect
         
         // >>>>>>>>>>>>>>>>> изменение цвета вью в rect площаде (2 спосооб)
         //CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
         //CGContextAddRect(context, rect);
         //CGContextFillPath(context);
         // <<<<<<<<<<<<<<<<<
         
         CGRect square1 = CGRectMake(50, 50, 130, 130);
         CGRect square2 = CGRectMake(180, 180, 130, 130);
         CGRect square3 = CGRectMake(310, 310, 130, 130);
         
         
         // >>>>>>>>>>>>>>>>>  вставить отдельные вью красного цвета
         //    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
         //    CGContextAddRect(context, square1);
         //    CGContextAddRect(context, square2);
         //    CGContextAddRect(context, square3);
         //
         //    CGContextFillPath(context);
         // <<<<<<<<<<<<<<<<<<
         
         
         // >>>>>>>>>>>>>>>>> вставить круглые заполненые зеленым цветом вью
         
         CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
         
         CGContextAddEllipseInRect(context, square1);
         CGContextAddEllipseInRect(context, square2);
         CGContextAddEllipseInRect(context, square3);
         
         CGContextFillPath(context);
         // <<<<<<<<<<<<<<<<
         
         // >>>>>>>>>>>>>>>>>> установка границ другим цветом
         
         CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
         
         CGContextAddRect(context, square1);
         CGContextAddRect(context, square2);
         CGContextAddRect(context, square3);
         
         CGContextStrokePath(context);
         
         CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
         CGContextAddEllipseInRect(context, square1);
         CGContextAddEllipseInRect(context, square2);
         CGContextAddEllipseInRect(context, square3);
         CGContextStrokePath(context);
         // <<<<<<<<<<<<<<<<<<<<<<
         
         // >>>>>>>>>>>>>>>>>>>>>>>> рисование линий
         CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
         // 1 - задать ширину линии
         CGContextSetLineWidth(context, 3.f);
         // 1.1 - установка конца линии круглой
         CGContextSetLineCap(context, kCGLineCapRound);
         // 2 - переместится в нужную точку
         CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
         // 3 - перемеситься в другую точку, но с рисованием линии
         CGContextAddLineToPoint(context, CGRectGetMinX(square3), CGRectGetMaxY(square3));
         CGContextStrokePath(context);
         //вторая линия
         CGContextMoveToPoint(context, CGRectGetMaxX(square3), CGRectGetMinY(square3));
         CGContextAddLineToPoint(context, CGRectGetMaxX(square1), CGRectGetMinY(square1));
         CGContextStrokePath(context);
         //третья линия
         CGContextMoveToPoint(context, CGRectGetMinX(square3), CGRectGetMaxY(square3));
         CGContextAddLineToPoint(context, CGRectGetMaxX(square3), CGRectGetMinY(square3));
         CGContextStrokePath(context);
         // <<<<<<<<<<<<<<<<<<<<<<<<
         
         // >>>>>>>>>>>>>>>>>>>>>>>> рисование куска круга
         
         CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
         CGContextAddArc(context, CGRectGetMaxX(square1), CGRectGetMaxY(square1), CGRectGetWidth(square1),
         M_PI, M_PI_2, YES);
         CGContextStrokePath(context);
         
         CGContextMoveToPoint(context, CGRectGetMaxX(square3), CGRectGetMinY(square3));
         CGContextAddArc(context, CGRectGetMinX(square3), CGRectGetMinY(square3), CGRectGetWidth(square3),
         0, M_PI, YES);
         CGContextStrokePath(context);
         
         // <<<<<<<<<<<<<<<<<<<<<<<<<
     
     
         // >>>>>>>>>>>>>>>>>>>>>>>>>> заливка окружности цветом
             // CGContextFillEllipseInRect(context, CGRectMake(center.x - radius, center.y - radius,
             // radius * 2, radius * 2));
     
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
     
         // >>>>>>>>>>>>>>>>>>>>>>>>>> заливка прямоугольника не правильной формы цветом
             CGPoint points[8] = {   CGPointMake(upperPoint), CGPointMake(200, 100),
             CGPointMake(200, 100), CGPointMake(250, 200),
             CGPointMake(250, 200), CGPointMake(150, 200),
             CGPointMake(150, 200), CGPointMake(100, 100)};
             CGContextStrokeLineSegments(context, points, 8);
     
             CGContextAddLines(context, points, 8);
             CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
             CGContextFillPath(context);
     
         // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
     
     
        // >>>>>>>>>>>>>>>>>>>>>>>>>> нарисовать треугольник
                 CGContextSetRGBStrokeColor(context, 255, 0, 255, 1);
                 CGPoint points[6] = {CGPointMake(70, 20), CGPointMake(120, 120),
                 CGPointMake(120, 120), CGPointMake(20, 120),
                 CGPointMake(20, 120), CGPointMake(70, 20)};
                 CGContextStrokeLineSegments(context, points, 6);
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
         
         
         // >>>>>>>>>>>>>>>>>>>>>>>>>>  вырисовка текста
         NSString * text = @"test";
         UIFont * font = [UIFont systemFontOfSize:20];   //сиситемный дефолтный шрифт
         NSShadow * shadow = [[NSShadow alloc] init];    //создание тени
         shadow.shadowOffset = CGSizeMake(1, 1);         //указуем сдвиг тени
         shadow.shadowColor = [UIColor blackColor];      //выбор цвета тени
         shadow.shadowBlurRadius = 0.5f;                 //размытость тени
         //установка атрибутов нашему тексту
         NSDictionary * attributes = [NSDictionary dictionaryWithObjectsAndKeys:
         [UIColor whiteColor],   NSForegroundColorAttributeName,             //у текста будет серый цвет
         font,                  NSFontAttributeName,                        //установка font
         shadow,                NSShadowAttributeName,                      //установка тени
         nil];
         
         //вырисовка текста 1 способ
         //вырисока текста в центре square2 (отнимаем половину textSize чтоб размещенеие было в самом центре)
         //    CGSize textSize = [text sizeWithAttributes:attributes]; //ширина текста с учетом всех атрибутов
         //    [text drawAtPoint:CGPointMake(CGRectGetMidX(square2) - textSize.width / 2,
         //                                  CGRectGetMidY(square2) - textSize.height / 2)
         //                                withAttributes:attributes];
         
         //вырисовка текста 2 способ - лучше, так как предотвращает размытие
         CGSize textSize = [text sizeWithAttributes:attributes];
         CGRect textRect = CGRectMake(CGRectGetMidX(square2) - textSize.width / 2, CGRectGetMidY(square2) - textSize.height / 2,
         textSize.width, textSize.height);
         textRect = CGRectIntegral(textRect);   //округляет значения хранящиеся в Rect - если было 12.3, то будет 12.0
         [text drawInRect:textRect withAttributes:attributes];
         
         
         // <<<<<<<<<<<<<<<<<<<<<<<<<<
     
        
        
        
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>> вырисовка шахматной доски chess board
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>> вырисовка шахматной доски
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>> вырисовка шахматной доски
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>> вырисовка шахматной доски
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGFloat border = 40;
        
        CGFloat maxBoardWidth = MIN(CGRectGetWidth(rect), CGRectGetHeight(rect));
        
        CGFloat heightIndent = (CGRectGetHeight(rect) - maxBoardWidth) / 2;
        CGFloat widthIndent = (CGRectGetWidth(rect) - maxBoardWidth) / 2;
        
        CGFloat boardWidth = maxBoardWidth - border * 2;
        
        int sizeCell = (int) boardWidth / 8;
        
        boardWidth = sizeCell * 8;
        
        border = (maxBoardWidth - boardWidth) / 2;
        
        CGRect board = CGRectMake(CGRectGetMinX(rect) + border + widthIndent, CGRectGetMinY(rect) + border + heightIndent,
                                  boardWidth, boardWidth);
        board = CGRectIntegral(board);
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextAddRect(context, board);
        CGContextFillPath(context);
        
        
        //draw cells
        CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
        for (NSInteger row = 0; row < 8; row++) {
            for (NSInteger cols = 0; cols < 8; cols ++) {
                if ( row % 2 != cols % 2   )
                {
                    CGContextAddRect(context, CGRectMake(CGRectGetMinX(board) + cols * sizeCell,
                                                         CGRectGetMinY(board) + row * sizeCell,
                                                         sizeCell, sizeCell));
                    CGContextFillPath(context);
                }
            }
        }
        // draw border
        CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
        CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
        CGContextSetLineWidth(context, 3.f);
        CGContextAddRect(context, board);
        CGContextStrokePath(context);
        
        //draw number and letter
        
        NSArray <NSString *> * arrayNumber = @[ @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8" ];
        NSArray <NSString *> * arrayLetter = @[ @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H" ];
        
        //    UIFont * font = [UIFont systemFontOfSize: 20];
        
        UIFont * font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:20];
        
        for (NSString * font in [UIFont familyNames] ) {
            NSLog(@"%@", font);
        }
        
        NSDictionary * attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIColor blackColor] , NSForegroundColorAttributeName,
                                     font,                  NSFontAttributeName,
                                     nil];
        
        for (NSInteger i = 7; i >= 0 ; i--)
        {
            // numbers from left
            CGSize textSizeNumberLeft = [arrayNumber[i]  sizeWithAttributes:attributes];
            CGRect textRectNumberLeft = CGRectMake(CGRectGetMinX(board) - border/2 - textSizeNumberLeft.width/2,
                                                   CGRectGetMinY(board) + sizeCell * (7 - i) + sizeCell/2 - textSizeNumberLeft.height/2 ,
                                                   textSizeNumberLeft.width, textSizeNumberLeft.height);
            textRectNumberLeft = CGRectIntegral(textRectNumberLeft);
            [arrayNumber[i] drawInRect:textRectNumberLeft withAttributes:attributes];
            
            
            //letters from down
            CGSize textSizeLetterDown = [arrayLetter[7 - i]  sizeWithAttributes:attributes];
            CGRect textRectLetterDown = CGRectMake(CGRectGetMinX(board) + sizeCell/2 - textSizeLetterDown.width /2 + sizeCell * (7 - i),
                                                   CGRectGetMaxY(board) + border / 2 - textSizeLetterDown.height / 2,
                                                   textSizeLetterDown.width, textSizeLetterDown.height);
            textRectLetterDown = CGRectIntegral(textRectLetterDown);
            [arrayLetter[7 - i] drawInRect:textRectLetterDown withAttributes:attributes];
            
            // numbers from right
            CGSize textSizeNumberRight = [arrayNumber[i]  sizeWithAttributes:attributes];
            CGRect textRectNumberRight = CGRectMake(CGRectGetMaxX(board) + border/2 - textSizeNumberRight.width/2,
                                                    CGRectGetMaxY(board) - sizeCell * (7 - i) - sizeCell/2 - textSizeNumberRight.height/2 ,
                                                    textSizeNumberRight.width, textSizeNumberRight.height);
            textRectNumberRight = CGRectIntegral(textRectNumberRight);
            [arrayNumber[i] drawInRect:textRectNumberRight withAttributes:attributes];
            
            //letters from up
            CGSize textSizeLetterUp = [arrayLetter[ i]  sizeWithAttributes:attributes];
            CGRect textRectLetterUp = CGRectMake(CGRectGetMinX(board) + sizeCell/2 - textSizeLetterUp.width /2 + sizeCell * (7 - i),
                                                 CGRectGetMinY(board) - border / 2 - textSizeLetterUp.height / 2,
                                                 textSizeLetterUp.width, textSizeLetterUp.height);
            textRectLetterUp = CGRectIntegral(textRectLetterUp);
            [arrayLetter[i] drawInRect:textRectLetterUp withAttributes:attributes];
            
        }
        
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        
        
        
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Распечатка всех шрифтов в UIFront
        
        //    CGFloat shiftY = 0.f;
        //    for (NSString * font in [UIFont familyNames] ) {
        //
        //        UIFont * currentFont = [UIFont fontWithName: font size:14];
        //
        //        NSString * string = [NSString stringWithFormat:@"%@ - \t\t\tabcd ABCD 1234567890", font];
        //        NSDictionary * attr = [NSDictionary dictionaryWithObjectsAndKeys:
        //                               currentFont , NSFontAttributeName,
        //                               [UIColor blackColor], NSForegroundColorAttributeName,
        //                               nil];
        //
        //        CGSize textSizeLetterDown = [string sizeWithAttributes:attr];
        //        CGRect textRectLetterDown = CGRectMake(CGRectGetMinX(rect) + 0,
        //                                               CGRectGetMinY(rect) + shiftY,
        //                                               textSizeLetterDown.width, textSizeLetterDown.height);
        //        textRectLetterDown = CGRectIntegral(textRectLetterDown);
        //        [string drawInRect:textRectLetterDown withAttributes:attr];
        //
        //        shiftY += 16;
        //    }
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
  
    }
    
    @end
*/
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 25 - UIButton
    /*
    - (void)viewDidLoad {
        [super viewDidLoad];
        // ***********************************************************************************************************************
////          энумы UIControl (супер класс для кнопок, слайдеров и т.д.)
//         typedef NS_OPTIONS(NSUInteger, UIControlEvents) {
//         UIControlEventTouchDown                                         = 1 <<  0,       on all touch downs
//         UIControlEventTouchDownRepeat                                   = 1 <<  1,       on multiple touchdowns (tap count > 1)
//         UIControlEventTouchDragInside                                   = 1 <<  2,
//         UIControlEventTouchDragOutside                                  = 1 <<  3,
//         UIControlEventTouchDragEnter                                    = 1 <<  4,
//         UIControlEventTouchDragExit                                     = 1 <<  5,
//         нажали и отпустили в этом же месте
//         UIControlEventTouchUpInside                                     = 1 <<  6,
//         нажали, провели пальцем и отпустили в другом месте
//         UIControlEventTouchUpOutside                                    = 1 <<  7,
//         UIControlEventTouchCancel                                       = 1 <<  8,
//
//         UIControlEventValueChanged                                      = 1 << 12,      sliders, etc.
//         UIControlEventPrimaryActionTriggered NS_ENUM_AVAILABLE_IOS(9_0) = 1 << 13,      semantic action: for buttons, etc.
//
//         UIControlEventEditingDidBegin                                   = 1 << 16,      UITextField
//         UIControlEventEditingChanged                                    = 1 << 17,
//         UIControlEventEditingDidEnd                                     = 1 << 18,
//         UIControlEventEditingDidEndOnExit                               = 1 << 19,      'return key' ending editing
//
//         UIControlEventAllTouchEvents                                    = 0x00000FFF,   for touch events
//         UIControlEventAllEditingEvents                                  = 0x000F0000,   for UITextField
//         UIControlEventApplicationReserved                               = 0x0F000000,   range available for application use
//         UIControlEventSystemReserved                                    = 0xF0000000,   range reserved for internal framework use
//         UIControlEventAllEvents                                         = 0xFFFFFFFF
//         };
//
//         typedef NS_ENUM(NSInteger, UIControlContentVerticalAlignment) {
//         UIControlContentVerticalAlignmentCenter  = 0,
//         UIControlContentVerticalAlignmentTop     = 1,
//         UIControlContentVerticalAlignmentBottom  = 2,
//         UIControlContentVerticalAlignmentFill    = 3,
//         };
//
//         typedef NS_ENUM(NSInteger, UIControlContentHorizontalAlignment) {
//         UIControlContentHorizontalAlignmentCenter = 0,
//         UIControlContentHorizontalAlignmentLeft   = 1,
//         UIControlContentHorizontalAlignmentRight  = 2,
//         UIControlContentHorizontalAlignmentFill   = 3,
//         UIControlContentHorizontalAlignmentLeading  API_AVAILABLE(ios(11.0), tvos(11.0)) = 4,
//         UIControlContentHorizontalAlignmentTrailing API_AVAILABLE(ios(11.0), tvos(11.0)) = 5,
//         };
//
//         typedef NS_OPTIONS(NSUInteger, UIControlState) {
//         UIControlStateNormal       = 0,
//          когда нажали на контрол и он засветился
//         UIControlStateHighlighted  = 1 << 0,                   used when UIControl isHighlighted is set
//         UIControlStateDisabled     = 1 << 1,
//          когда включена кнопка!
//         UIControlStateSelected     = 1 << 2,                   flag usable by app (see below)
//         UIControlStateFocused NS_ENUM_AVAILABLE_IOS(9_0) = 1 << 3,  Applicable only when the screen supports focus
//         UIControlStateApplication  = 0x00FF0000,               additional flags available for application use
//         UIControlStateReserved     = 0xFF000000                flags reserved for internal framework use
//         };
         // ***********************************************************************************************************************
        
        // энумы UIButton
        
//         typedef NS_ENUM(NSInteger, UIButtonType) {
//         // кастомная кнопка
//         UIButtonTypeCustom = 0,                         // no button type
//         UIButtonTypeSystem NS_ENUM_AVAILABLE_IOS(7_0),  // standard system button
//
//         UIButtonTypeDetailDisclosure,
//         UIButtonTypeInfoLight,
//         UIButtonTypeInfoDark,
//         // c плюсиком кнопка
//         UIButtonTypeContactAdd,
//
//         UIButtonTypePlain API_AVAILABLE(tvos(11.0)) API_UNAVAILABLE(ios, watchos), // standard system button without the blurred background view
//
//         UIButtonTypeRoundedRect = UIButtonTypeSystem   // Deprecated, use UIButtonTypeSystem instead
//         };
        // ***********************************************************************************************************************
        
        
        // константные строки для NSDictionary от NSAtributeString
        
        
        
         
//         // Predefined character attributes for text. If the key is not present in the dictionary, it indicates the default value described below.
//         UIKIT_EXTERN NSAttributedStringKey const NSFontAttributeName NS_AVAILABLE(10_0, 6_0);                // UIFont, default Helvetica(Neue) 12
//         UIKIT_EXTERN NSAttributedStringKey const NSParagraphStyleAttributeName NS_AVAILABLE(10_0, 6_0);      // NSParagraphStyle, default defaultParagraphStyle
//         UIKIT_EXTERN NSAttributedStringKey const NSForegroundColorAttributeName NS_AVAILABLE(10_0, 6_0);     // UIColor, default blackColor
//         UIKIT_EXTERN NSAttributedStringKey const NSBackgroundColorAttributeName NS_AVAILABLE(10_0, 6_0);     // UIColor, default nil: no background
//         UIKIT_EXTERN NSAttributedStringKey const NSLigatureAttributeName NS_AVAILABLE(10_0, 6_0);            // NSNumber containing integer, default 1: default ligatures, 0: no ligatures
//         UIKIT_EXTERN NSAttributedStringKey const NSKernAttributeName NS_AVAILABLE(10_0, 6_0);                // NSNumber containing floating point value, in points; amount to modify default kerning. 0 means kerning is disabled.
//         UIKIT_EXTERN NSAttributedStringKey const NSStrikethroughStyleAttributeName NS_AVAILABLE(10_0, 6_0);  // NSNumber containing integer, default 0: no strikethrough
//         UIKIT_EXTERN NSAttributedStringKey const NSUnderlineStyleAttributeName NS_AVAILABLE(10_0, 6_0);      // NSNumber containing integer, default 0: no underline
//         UIKIT_EXTERN NSAttributedStringKey const NSStrokeColorAttributeName NS_AVAILABLE(10_0, 6_0);         // UIColor, default nil: same as foreground color
//         UIKIT_EXTERN NSAttributedStringKey const NSStrokeWidthAttributeName NS_AVAILABLE(10_0, 6_0);         // NSNumber containing floating point value, in percent of font point size, default 0: no stroke; positive for stroke alone, negative for stroke and fill (a typical value for outlined text would be 3.0)
//         UIKIT_EXTERN NSAttributedStringKey const NSShadowAttributeName NS_AVAILABLE(10_0, 6_0);              // NSShadow, default nil: no shadow
//         UIKIT_EXTERN NSAttributedStringKey const NSTextEffectAttributeName NS_AVAILABLE(10_10, 7_0);          // NSString, default nil: no text effect
//
//         UIKIT_EXTERN NSAttributedStringKey const NSAttachmentAttributeName NS_AVAILABLE(10_0, 7_0);          // NSTextAttachment, default nil
//         UIKIT_EXTERN NSAttributedStringKey const NSLinkAttributeName NS_AVAILABLE(10_0, 7_0);                // NSURL (preferred) or NSString
//         UIKIT_EXTERN NSAttributedStringKey const NSBaselineOffsetAttributeName NS_AVAILABLE(10_0, 7_0);      // NSNumber containing floating point value, in points; offset from baseline, default 0
//         UIKIT_EXTERN NSAttributedStringKey const NSUnderlineColorAttributeName NS_AVAILABLE(10_0, 7_0);      // UIColor, default nil: same as foreground color
//         UIKIT_EXTERN NSAttributedStringKey const NSStrikethroughColorAttributeName NS_AVAILABLE(10_0, 7_0);  // UIColor, default nil: same as foreground color
//         UIKIT_EXTERN NSAttributedStringKey const NSObliquenessAttributeName NS_AVAILABLE(10_0, 7_0);         // NSNumber containing floating point value; skew to be applied to glyphs, default 0: no skew
//         UIKIT_EXTERN NSAttributedStringKey const NSExpansionAttributeName NS_AVAILABLE(10_0, 7_0);           // NSNumber containing floating point value; log of expansion factor to be applied to glyphs, default 0: no expansion
//
//         UIKIT_EXTERN NSAttributedStringKey const NSWritingDirectionAttributeName NS_AVAILABLE(10_6, 7_0);    // NSArray of NSNumbers representing the nested levels of writing direction overrides as defined by Unicode LRE, RLE, LRO, and RLO characters.  The control characters can be obtained by masking NSWritingDirection and NSWritingDirectionFormatType values.  LRE: NSWritingDirectionLeftToRight|NSWritingDirectionEmbedding, RLE: NSWritingDirectionRightToLeft|NSWritingDirectionEmbedding, LRO: NSWritingDirectionLeftToRight|NSWritingDirectionOverride, RLO: NSWritingDirectionRightToLeft|NSWritingDirectionOverride,
//
//         UIKIT_EXTERN NSAttributedStringKey const NSVerticalGlyphFormAttributeName NS_AVAILABLE(10_7, 6_0);   // An NSNumber containing an integer value.  0 means horizontal text.  1 indicates vertical text.  If not specified, it could follow higher-level vertical orientation settings.  Currently on iOS, it's always horizontal.  The behavior for any other value is undefined.
        
        
        UIButton * button = [UIButton buttonWithType: UIButtonTypeCustom];
        
        button.frame  = CGRectMake(100, 200, 200, 200);
        button.backgroundColor = [UIColor lightGrayColor];
        
        
        //    [button setTitle: @"Button" forState: UIControlStateNormal ]; // UIControlStateNormal - нормальное состояние кнопки
        //    [button setTitle: @"ButtonPressed" forState: UIControlStateHighlighted ];
        //    [button setTitleColor: [UIColor blueColor] forState: UIControlStateNormal]; //установить синий цвет надписи в нормальном состоянии
        //    [button setTitleColor: [UIColor greenColor] forState: UIControlStateHighlighted]; //установить зеленый цвет при нажатии
        
        
        
        // создание атрибутов для шрифта через NSDictionare
        NSDictionary * attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:30],
                                      NSForegroundColorAttributeName : [UIColor orangeColor],
                                      };
        
        NSAttributedString * tittle = [[NSAttributedString alloc] initWithString:@"Button" attributes:attributes];
        
        [button setAttributedTitle:tittle forState:UIControlStateNormal];
        
        NSDictionary * attributes2 = @{NSFontAttributeName: [UIFont systemFontOfSize:20],
                                       NSForegroundColorAttributeName : [UIColor blackColor],
                                       };
        
        NSAttributedString * tittle2 = [[NSAttributedString alloc] initWithString:@"ButtonPressed" attributes:attributes2];
        
        [button setAttributedTitle:tittle2 forState:UIControlStateHighlighted];
        
        
        //отступ внутри (структура)
        UIEdgeInsets insets = UIEdgeInsetsMake(100, 100, 0, 0);
        button.titleEdgeInsets = insets;
        
        [self.view addSubview: button];
        
        [button addTarget: self action: @selector(actionTest:) forControlEvents: UIControlEventTouchUpInside];
        [button addTarget: self action: @selector(actionTestOutside:) forControlEvents: UIControlEventTouchUpOutside];
    }
    
#pragma mark - Actions
    
    - (void) actionTest: (UIButton *) but
    {
        NSLog(@"Button Pressed!");
    }
    
    - (void) actionTestOutside: (UIButton *) but
    {
        NSLog(@"Button Pressed Outside!");
    }
    
    
    - (IBAction)actionTest2:(UIButton *)sender forEvent:(UIEvent *)event {
        if (event.type == UIEventTypeMotion)
        {
            NSLog(@"button2 action");
        }
    }
*/
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 27 - UITextField Part 1
/*
    @interface ViewController () <UITextFieldDelegate>
    - (IBAction)actionTextChanged:(UITextField *)sender;
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        //    self.firstNameField.keyboardAppearance = UIKeyboardAppearanceDark;
        //    self.lastNameField.keyboardAppearance = UIKeyboardAppearanceLight;
        
        self.firstNameField.delegate = self;
        self.lastNameField.delegate = self;
        
        [self.firstNameField becomeFirstResponder]; // при запуске приложения будет активное ферстНэймОкно с клавиатурой
        
        
        NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(notificationTextDidBeginEditing:)
                   name:UITextFieldTextDidBeginEditingNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(notificationTextDidEndEditing:)
                   name:UITextFieldTextDidEndEditingNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(notificationTextDidChangeEditing:)
                   name:UITextFieldTextDidChangeNotification
                 object:nil];
        
        //нотификации
        
    }
    
    // очитска нотификации, чтоб они не остались, когда контролер уничтожиться
    - (void) dealloc
    {
        [[NSNotificationCenter defaultCenter] removeObserver: self];
    }
    
#pragma mark - Actions
    
    - (void)actionLog:(id)sender
    {
        NSLog(@"First Name = %@, Last Name = %@", self.firstNameField.text, self.lastNameField.text);
        
        if ([self.firstNameField isFirstResponder])
            [self.firstNameField resignFirstResponder];
        else if ([self.lastNameField isFirstResponder])
            [self.lastNameField resignFirstResponder];
    }
    
    
    
    - (IBAction)actionTextChanged:(UITextField *)sender {
        NSLog(@"%@", sender.text);
    }
    
#pragma marks - UITextFieldDelegate
    // разрешать или не разрешать очищать поле
    - (BOOL)textFieldShouldClear:(UITextField *)textField {
        return NO;
    }
    
    //- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
    //{
    //    //return [textField isEqual: self.firstNameField];
    //}
    
    // реагирование на нажатие кнопки RETURN
    - (BOOL)textFieldShouldReturn:(UITextField *)textField {
        
        if ( [textField isEqual: self.firstNameField])
        {
            [self.lastNameField becomeFirstResponder];  //перенести а ктивность курсора в ласт нэйм
        }
        else
        {
            [textField resignFirstResponder]; // убрать активность ввода вообще (срабатывает при вводе в ласт нэйм)
        }
        
        return YES;
    }
    
    
#pragma mark - Notifications
    
    - (void) notificationTextDidBeginEditing: (NSNotification * ) notification
    {
        NSLog(@"TextDidBeginEditing");
    }
    
    - (void) notificationTextDidEndEditing: (NSNotification * ) notification
    {
        NSLog(@"TextDidEndEditing");
    }
    
    - (void) notificationTextDidChangeEditing: (NSNotification * ) notification
    {
        NSLog(@"TextDidChangeEditing");
        NSLog(@"userInfo = %@", notification.userInfo);
    }

    @end
 
 */
    
    // ***********************************************************************************************************************
#pragma mark - Lesson 28 - UITextField Part 2 - input Phone Number
    
  /*
    @interface ViewController ()
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
    }
    
#pragma mark - UITextFieldDelegate
    
    - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
    {
        NSLog(@"textField text = %@", textField.text);
        NSLog(@"shouldChangeCharactersInRange %@", NSStringFromRange(range));
        NSLog(@"replacementString %@", string);
        
        NSCharacterSet * validationSet  = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray * components = [string componentsSeparatedByCharactersInSet: validationSet];
        if (components.count > 1)
        {
            return NO;
        }
        
        // пример номера телефона
        //     +XX (XXX) XXX-XXXX
        
        NSString * newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"new string = %@", newString);
        
        //>>>>>>>>>>>>>>> убираем со строки - скобки, плюсы и т.д. - оставляем только цифры
        NSArray * validComponents = [newString componentsSeparatedByCharactersInSet: validationSet];
        newString  = [validComponents componentsJoinedByString:@""];
        // <<<<<<<<<<<<<<<
        NSLog(@"new string fixed = %@", newString);
        
        
        
        NSMutableString * resultString = [NSMutableString string];
        static const int localNumberMaxLength   = 7;
        static const int areaCodeMaxLength      = 3;
        static const int countryCodeMaxLength   = 3;
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength)
            return NO;
        
        NSInteger localNumberLength = MIN ([newString length], localNumberMaxLength);
        
        if(localNumberLength > 0){
            NSString * number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
            [resultString appendString: number];
            if ([resultString length] > 3)
            {
                [resultString insertString:@"-" atIndex:3];
            }
        }
        
        if ([newString length] > localNumberMaxLength)
        {
            NSInteger areaCodeLength = MIN ((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
            NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
            NSString * area = [newString substringWithRange:areaRange];
            area = [NSString stringWithFormat:@"(%@)", area];
            [resultString insertString: area atIndex:0];
        }
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength)
        {
            NSInteger countryCodeLength = MIN ((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            NSString * countryCode = [newString substringWithRange: countryCodeRange ];
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            [resultString insertString: countryCode atIndex:0];
        }
        
        NSLog(@"result string = %@", resultString);
        
        
        textField.text = resultString;
        
        return NO;
        
        
        //    NSCharacterSet * set = [NSCharacterSet characterSetWithCharactersInString:@" ,?"];
        
        //    NSString * resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        //    NSCharacterSet * set = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];  // все кроме цифр
        //    NSArray * words = [resultString componentsSeparatedByCharactersInSet:set];  //все что не является цифрой будет разделять на слова
        //    NSLog(@"words = %@", words);
        //    return [resultString length] <= 10;
        //    return [resultString length] <= 10;
    }
    */
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 29 - TableView Static Cells, NSUserDefaults
    
    
/*
#import "SettingViewController.h"
    
    @interface SettingViewController ()
    
    @property (weak, nonatomic) IBOutlet UITextField * loginField;
    @property (weak, nonatomic) IBOutlet UITextField *passwordField;
    @property (weak, nonatomic) IBOutlet UISegmentedControl *levelControl;
    @property (weak, nonatomic) IBOutlet UISwitch *shadowsSwitch;
    @property (weak, nonatomic) IBOutlet UISegmentedControl *detalizationsControl;
    @property (weak, nonatomic) IBOutlet UISlider *soundSlider;
    @property (weak, nonatomic) IBOutlet UISlider *musicSlider;
    //@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
    
    @end
    
    static NSString * kSettingsLogin        = @"login";
    static NSString * kSettingsPassword     = @"password";
    static NSString * kSettingsLevel        = @"level";
    static NSString * kSettingsShadows      = @"shadows";
    static NSString * kSettingsDetalization = @"detalization";
    static NSString * kSettingsSound        = @"sound";
    static NSString * kSettingsMusic        = @"music";
    
    @implementation SettingViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        [self loadSettings];
        
        // подписка на нотификации, для того чтобы сделать, чтоб клавиатура (keyboard) не закрывала собой TableView
        NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(notificationKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [nc addObserver:self selector:@selector(notificationKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        
        
        
    }
    
    - (void)dealloc
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
#pragma mark - Notification Methods
    
    - (void) notificationKeyboardWillShow: (NSNotification *) notfication
    {
        NSLog(@"notificationKeyboardWillShow: \n%@", notfication.userInfo);
    }
    
    - (void) notificationKeyboardWillHide: (NSNotification *) notfication
    {
        NSLog(@"notificationKeyboardWillHide: \n%@", notfication.userInfo);
    }
    
#pragma mark - Save and Load
    
    - (void) saveSettings {
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        
        [userDefaults setObject: self.loginField.text                   forKey: kSettingsLogin];
        [userDefaults setObject: self.passwordField.text                forKey: kSettingsPassword];
        
        [userDefaults setInteger: self.levelControl.selectedSegmentIndex forKey: kSettingsLevel];
        [userDefaults setBool: self.shadowsSwitch.isOn               forKey: kSettingsShadows];
        [userDefaults setInteger: self.detalizationsControl.selectedSegmentIndex forKey: kSettingsDetalization];
        [userDefaults setDouble:self.soundSlider.value forKey:kSettingsSound];
        [userDefaults setDouble:self.musicSlider.value forKey:kSettingsMusic];
        
        
        // используется для сохранения настроек при внезапном выходе. В документации пишут, что он будет скоро деприкейтед
        [userDefaults synchronize];
    }
    
    - (void) loadSettings {
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        
        self.loginField.text =                      [userDefaults       objectForKey: kSettingsLogin];
        self.passwordField.text =                   [userDefaults       objectForKey: kSettingsPassword];
        self.levelControl.selectedSegmentIndex =    [userDefaults       integerForKey:kSettingsLevel];
        self.shadowsSwitch.on =                     [userDefaults       boolForKey:kSettingsShadows];
        self.detalizationsControl.selectedSegmentIndex = [userDefaults  integerForKey:kSettingsDetalization];
        self.soundSlider.value =                    [userDefaults       doubleForKey:kSettingsSound];
        self.musicSlider.value =                    [userDefaults       doubleForKey:kSettingsMusic];
    }
    
#pragma mark - Actions
    
    - (IBAction)actionTextChanged:(UITextField *)sender {
        [self saveSettings];
    }
    
    - (IBAction)actionValueChanged:(id)sender {
        [self saveSettings];
    }
 
 */

    
// ***********************************************************************************************************************
#pragma mark - Lesson 30-31 - TableView editing, paste rows and section, remove
    
    /*
#import "ViewController.h"
    
#import "ASGroup.h"
#import "ASStudent.h"
    
    @interface ViewController () <UITableViewDelegate, UITableViewDataSource>
    
    @property (weak, nonatomic) UITableView * tableView;
    @property (strong, nonatomic) NSMutableArray * groupsArray;
    
    @end
    
    @implementation ViewController
    
    - (void) loadView
    {
        [super loadView];
        CGRect frame = self.view.bounds;
        frame.origin = CGPointZero;
        
        UITableView * tableView = [[UITableView alloc] initWithFrame:frame style: UITableViewStyleGrouped];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        tableView.delegate = self;
        tableView.dataSource = self;
        
        [self.view addSubview:tableView];
        self.tableView = tableView;
        
        // включить функции редоктирования - передвигания и кнопка появлется удаление
        //tableView.editing = YES;
        
        
        // в режиме редоктирования есть возможность нажать на ячейку
        self.tableView.allowsSelectionDuringEditing = YES;
        
        
        
    }
    
    - (void)viewDidLoad
    {
        [super viewDidLoad];
        
        self.groupsArray = [NSMutableArray array];
        
        for (int i = 0; i < arc4random() % 6 + 5; i++)
        {
            ASGroup * group = [[ASGroup alloc] init];
            group.name = [NSString stringWithFormat:@"Group #%i", i];
            NSMutableArray * array = [NSMutableArray array];
            for (int j = 0; j < arc4random() % 11 + 15; j++) {
                [array addObject: [ASStudent randomStudent]];
            }
            group.students = array;
            [self.groupsArray addObject:group];
        }
        
        [self.tableView reloadData];
        
        
        // присваиваем title для navigation bar
        self.navigationItem.title = @"Students";
        
        // добавление кнопок на навигэйшн бар
        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(actionEdit:)];
        self.navigationItem.rightBarButtonItem = editButton;
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(actionAddSection:)];
        self.navigationItem.leftBarButtonItem = addButton;
        
        
        
        
    }
    
#pragma mark - Actions
    
    - (void) actionAddSection: (UIBarButtonItem *) sender
    {
        ASGroup * group = [[ASGroup alloc] init];
        group.name = [NSString stringWithFormat:@"Group #%li", [self.groupsArray count] + 1];
        group.students = @[[ASStudent randomStudent],[ASStudent randomStudent]];
        NSInteger newSectionIndex = 0;
        [self.groupsArray insertObject:group atIndex:newSectionIndex];
        NSIndexSet * insertSections = [NSIndexSet indexSetWithIndex: newSectionIndex];
        
        
        // анимациооное добавление новых секций!!!!
        [self.tableView beginUpdates];
        
        [self.tableView insertSections: insertSections withRowAnimation:UITableViewRowAnimationLeft];
        
        [self.tableView endUpdates];
        
        
        // >>>>>>>>>>>>>>>>>>>>>>>>>>> - метод, чтоб новые секции не вылетали хаотично, а по одному через 0.3 сек
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];   //игнорировать любые нажатия
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            }
        });
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }
    
    - (void) actionEdit: (UIBarButtonItem *) sender
    {
        BOOL isEditing = self.tableView.editing;
        [self.tableView setEditing: !isEditing animated:YES];
        
        UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
        
        if (self.tableView.editing) {
            item = UIBarButtonSystemItemDone;
        }
        
        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                     target:self
                                                                                     action:@selector(actionEdit:)];
        // изменение кнопки без анимации
        //self.navigationItem.rightBarButtonItem = editButton;
        
        //изменение кнопки с анимацией
        [self.navigationItem setRightBarButtonItem:editButton animated:YES];
    }
    
#pragma mark - UITableViewDataSource
    
    // удаление ячейки
    - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            ASGroup * sourceGroup = [self.groupsArray objectAtIndex: indexPath.section];
            ASStudent * student = [sourceGroup.students objectAtIndex:indexPath.row - 1];
            NSMutableArray * tempArray = [NSMutableArray arrayWithArray:sourceGroup.students];
            [tempArray removeObject: student];
            sourceGroup.students = tempArray;
            
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            [tableView endUpdates];
            
        }
    }
    
    // целевой Index Path в который передвигается ячейка proposedDestinationIndexPath с sourceIndexPath
    - (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
    {
        if (proposedDestinationIndexPath.row == 0)
        {
            return sourceIndexPath;
        }
        else return proposedDestinationIndexPath;
        
    }
    
    // разрешить или запретить двигать ячейку
    - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
    {
//
//         ASGroup * sourceGroup = [self.groupsArray objectAtIndex: indexPath.section];
//         ASStudent * student = [sourceGroup.students objectAtIndex: indexPath.row];
//
//         return student.averageGrade < 4.f;
//
        if (indexPath.row == 0)
            return NO;
        else
            return YES;
    }
    
    // передвижения ячейки с исходящего IndexPath - sourceIndexPath в целевую ячейку -destinationIndexPath
    - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
    {
        ASGroup * sourceGroup = [self.groupsArray objectAtIndex: sourceIndexPath.section];
        ASStudent * student = [sourceGroup.students objectAtIndex: sourceIndexPath.row - 1];
        
        NSMutableArray * tempArray = [NSMutableArray arrayWithArray:sourceGroup.students];
        
        if (sourceIndexPath.section == destinationIndexPath.section) {
            [tempArray exchangeObjectAtIndex:sourceIndexPath.row - 1 withObjectAtIndex:destinationIndexPath.row - 1];
            sourceGroup.students = tempArray;
        } else
        {
            [tempArray removeObject:student];
            sourceGroup.students = tempArray;
            
            ASGroup * destinationGroup = [self.groupsArray objectAtIndex: destinationIndexPath.section];
            tempArray = [NSMutableArray arrayWithArray:destinationGroup.students];
            [tempArray insertObject:student atIndex:destinationIndexPath.row - 1];
            
            destinationGroup.students = tempArray;
        }
        
        
    }
    
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        ASGroup * group = [self.groupsArray objectAtIndex:section];
        return [group.students count] + 1;
        
    }
    
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        if (indexPath.row == 0)
        {
            static NSString * addStudentIdentifaer = @"AddStudentCell";
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier: addStudentIdentifaer];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addStudentIdentifaer];
                cell.textLabel.textColor = [UIColor blueColor];
                cell.textLabel.text = @"Tap to add new student";
            }
            return cell;
            
        }
        else
        {
            
            static NSString * studentIdentifer = @"StudentCell";
            
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier: studentIdentifer];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:studentIdentifer];
            }
            
            ASGroup * group = [self.groupsArray objectAtIndex: indexPath.section];
            ASStudent * student = [group.students objectAtIndex:indexPath.row - 1];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%1.2f", student.averageGrade];
            
            if (student.averageGrade >= 4.0f)
            {
                cell.detailTextLabel.textColor = [UIColor greenColor];
            } else if (student.averageGrade >= 3.0f) {
                cell.detailTextLabel.textColor = [UIColor orangeColor];
            }
            else
                cell.detailTextLabel.textColor = [UIColor redColor];
            
            return cell;
        }
    }
    
    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return [self.groupsArray count];
    }
    
    - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
    {
        return [(ASGroup *)[self.groupsArray objectAtIndex:section] name];
    }
    
    
    
#pragma mark - UITableViewDelegate
    
    
    
    // название кнопки (которая всплывает справа) при удалении ячейки (по умолчании Delete)
    - (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return  @"Remove";
    }
    
    // убрать красный кружек - который используется для удаление (слева в таблице)
    - (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        //return  UITableViewCellEditingStyleNone;
        //return ((arc4random() % 1000) / 500) ?  UITableViewCellEditingStyleInsert : UITableViewCellEditingStyleDelete;
        return indexPath.row == 0 ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete;
    }
    
    // предотвратить отступ слева от title ячейки (после того как убрали красный кружочек для удаления)
    - (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return NO;
    }
    
    // обработка нажатия на ячейку для добавления студентов
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        if (indexPath.row == 0) {
            ASGroup * group = [self.groupsArray objectAtIndex:indexPath.section];
            NSMutableArray * tempArray = nil;
            if (group.students)
            {
                tempArray = [NSMutableArray arrayWithArray:group.students];
            }
            else
                tempArray = [NSMutableArray array];
            
            NSInteger newStudentIndex = 0;
            [tempArray insertObject:[ASStudent randomStudent] atIndex: newStudentIndex];
            group.students = tempArray;
            
            NSIndexPath * newIndexPath = [NSIndexPath indexPathForItem: newStudentIndex + 1 inSection:indexPath.section];
            
            // анимациооное добавление новых рядов!!!!
            [self.tableView beginUpdates];
            
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            
            [self.tableView endUpdates];
            
            
            // >>>>>>>>>>>>>>>>>>>>>>>>>>> - метод, чтоб новые секции не вылетали хаотично, а по одному через 0.3 сек
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];   //игнорировать любые нажатия
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
                {
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                }
            });
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<
            
            
        }
    }
 
    @end
*/

    
    
    
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 32 - TableView editing - FileManager

    /*
#import "ASDirectoryViewController.h"
    
    @interface ASDirectoryViewController ()
    @property (strong, nonatomic) NSString * path;
    @property (strong, nonatomic) NSArray * contents;
    @end
    
    @implementation ASDirectoryViewController
    
    
    - (instancetype)initWitthFolderPath:(NSString *)path
    {
        self = [super initWithStyle: UITableViewStyleGrouped];
        if (self) {
            self.path = path;
            
            self.contents = [self contentsOfPath:path];;
            
        }
        return self;
    }
    
    
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        self.navigationItem.title = [self.path lastPathComponent];
        
        // установка custom кнопки на навигации - кнопка домой
        //    self.navigationItem.backBarButtonItem =
        //    [[UIBarButtonItem alloc] initWithTitle:@"Back"
        //                                     style:UIBarButtonItemStylePlain
        //                                    target:nil
        //                                    action:nil];
        
        if (self != [self.navigationController.viewControllers objectAtIndex:0])
        {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]   initWithTitle:@"ROOT"
                                                                                        style:UIBarButtonItemStylePlain
                                                                                       target:self
                                                                                       action:@selector(actionBackToRoot:)];
        }
    }
    
    - (void)viewDidAppear:(BOOL)animated
    {
        [super viewDidAppear:animated];
        NSLog(@"path - %@", self.path);
        NSLog(@"Count of stack view cintrollers - %li", [self.navigationController.viewControllers count]);
        NSLog(@"Carrent stack count - %li", [self.navigationController.viewControllers indexOfObject:self]);
    }
    
#pragma mark - Actions
    
    - (void) actionBackToRoot: (UIBarButtonItem *) sender
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
#pragma mark - Private Methods
    
    - (BOOL)isDirectoryAtIndexPath: (NSIndexPath * _Nonnull)indexPath {
        
        NSString * fileName = [self.contents objectAtIndex:indexPath.row];
        NSString * filePath = [self.path stringByAppendingPathComponent:fileName];
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        return isDirectory;
    }
    
    - (NSMutableArray *)contentsOfPath:(NSString *)path {
        NSError * error = nil;
        NSArray * urlArray = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[NSURL fileURLWithPath:path]
                                                           includingPropertiesForKeys:nil
                                                                              options:NSDirectoryEnumerationSkipsHiddenFiles
                                                                                error:&error];
        NSMutableArray * tempArray = [[NSMutableArray alloc] init];
        for (NSURL * url in urlArray) {
            
            [tempArray addObject:  [url lastPathComponent]];
        }
        
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        return tempArray;
    }
    
    
    
#pragma mark - UITableViewDataSource
    
    
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [self.contents count];
    }
    
    
    
    
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString * identifier = @"Cell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        NSString * fileName = [self.contents objectAtIndex:indexPath.row];
        
        cell.textLabel.text = fileName;
        
        if ([self isDirectoryAtIndexPath:indexPath])
        {
            cell.imageView.image = [UIImage imageNamed:@"folder.png"];
        }
        else
            cell.imageView.image = [UIImage imageNamed:@"file.png"];
        
        return cell;
    }
    
#pragma  mark - TableViewDelegate
    
    
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if ([self isDirectoryAtIndexPath:indexPath])
        {
            NSString * fileName = [self.contents objectAtIndex:indexPath.row];
            NSString * path = [self.path stringByAppendingPathComponent:fileName];
            
            ASDirectoryViewController * vc = [[ASDirectoryViewController alloc] initWitthFolderPath:path];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    @end
     
     */
    
// ***********************************************************************************************************************
#pragma mark - Lesson 34 - TableView editing - FileManager (make from storyboard)
/*
#import "ASDirectoryViewController.h"
#import "FileCell.h"
#import "UIView+UITableViewCell.h"
    
    @interface ASDirectoryViewController ()
    
    @property (strong, nonatomic) NSArray * contents;
    
    @property (strong, nonatomic) NSString * selectedPath;
    @end
    
    
    
    @implementation ASDirectoryViewController
    
    
    - (instancetype)initWitthFolderPath:(NSString *)path
    {
        self = [super initWithStyle: UITableViewStyleGrouped];
        if (self) {
            self.path = path;
        }
        return self;
    }
    
    
    - (void) setPath:(NSString *)path {
        _path = path;
        
        _contents = [self contentsOfPath:path];
        
        self.navigationItem.title = [self.path lastPathComponent];
        
        [self.tableView reloadData];
    }
    
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        if (self != [self.navigationController.viewControllers objectAtIndex:0])
        {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]   initWithTitle:@"ROOT"
                                                                                        style:UIBarButtonItemStylePlain
                                                                                       target:self
                                                                                       action:@selector(actionBackToRoot:)];
        }
        
        if (!self.path) {
            self.path = @"/Users/dmitriy/Downloads";
        }
    }
    
    - (void)viewDidAppear:(BOOL)animated
    {
        [super viewDidAppear:animated];
        NSLog(@"path - %@", self.path);
        NSLog(@"Count of stack view cintrollers - %li", [self.navigationController.viewControllers count]);
        NSLog(@"Carrent stack count - %li", [self.navigationController.viewControllers indexOfObject:self]);
    }
    
#pragma mark - Actions
    
    - (void) actionBackToRoot: (UIBarButtonItem *) sender
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
    - (IBAction)actionInfoCell:(UIButton *)sender {
        NSLog(@"actionInfoCell");
        UITableViewCell * cell = [sender superCell];
        
        if (cell)
        {
            NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
            NSLog(@"Action %li %li", indexPath.section, indexPath.row);
            
            NSString * path = [self.path stringByAppendingPathComponent: [self.contents objectAtIndex: indexPath.row]];
            //NSDictionary * attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:<#(nonnull NSString *)#> error:nil];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle: [NSString stringWithFormat:@"Action %li %li", indexPath.section, indexPath.row]
                                         message:[NSString stringWithFormat:@"%@", path]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                       }];
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    
#pragma mark - Private Methods
    
    - (BOOL)isDirectoryAtIndexPath: (NSIndexPath * _Nonnull)indexPath {
        
        NSString * fileName = [self.contents objectAtIndex:indexPath.row];
        NSString * filePath = [self.path stringByAppendingPathComponent:fileName];
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        return isDirectory;
    }
    
    - (NSMutableArray *)contentsOfPath:(NSString *)path {
        NSError * error = nil;
        NSArray * urlArray = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[NSURL fileURLWithPath:path]
                                                           includingPropertiesForKeys:nil
                                                                              options:NSDirectoryEnumerationSkipsHiddenFiles
                                                                                error:&error];
        NSMutableArray * tempArray = [[NSMutableArray alloc] init];
        for (NSURL * url in urlArray) {
            
            [tempArray addObject:  [url lastPathComponent]];
        }
        
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        return tempArray;
    }
    
    - (NSString *) fileSizeFromValue: (unsigned long long) size {
        static NSString * units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
        static unsigned unitsCount = 5;
        int index = 0;
        double fileSize = (double) size;
        
        while (fileSize > 1024 && index < unitsCount) {
            fileSize = fileSize / 1024;
            index++;
        }
        return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
    }
    
    
    
#pragma mark - UITableViewDataSource
    
    
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [self.contents count];
    }
    
    
    
    
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>    ячейки кастомные
        static NSString * fileIdentifier = @"FileCell";
        static NSString * folderIdentifier = @"FolderCell";
        
        NSString * fileName = [self.contents objectAtIndex:indexPath.row];
        
        if ([self isDirectoryAtIndexPath:indexPath])
        {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:folderIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:folderIdentifier];
            }
            cell.textLabel.text = fileName;
            return cell;
        }
        else
        {
            NSString * path = [self.path stringByAppendingPathComponent:fileName];
            NSDictionary * attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
            FileCell * cell = [tableView dequeueReusableCellWithIdentifier:fileIdentifier];
            if (!cell)
            {
                cell = [[FileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:fileIdentifier];
            }
            
            cell.nameLabel.text = fileName;
            cell.sizeLabel.text = [self fileSizeFromValue:[attributes fileSize]];
            
            static NSDateFormatter * dateFormater = nil;
            
            if (!dateFormater)
            {
                dateFormater = [[NSDateFormatter alloc] init];
                [dateFormater setDateFormat:@"MM/dd/yyyy hh:mm a"];
            }
            
            cell.dateLabel.text = [dateFormater stringFromDate:[attributes fileModificationDate]];
            return cell;
            
        }
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        // >>>>>>>>>>>>>>>>>>>>>>  ячейки системные (без кастомных)
        //    static NSString * identifier = @"Cell";
        //    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        //    if (!cell)
        //    {
        //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        //    }
        //
        //
        //    NSString * fileName = [self.contents objectAtIndex:indexPath.row];
        //
        //    cell.textLabel.text = fileName;
        //
        //    if ([self isDirectoryAtIndexPath:indexPath])
        //    {
        //        cell.imageView.image = [UIImage imageNamed:@"folder.png"];
        //    }
        //    else
        //        cell.imageView.image = [UIImage imageNamed:@"file.png"];
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        return nil;
    }
    
#pragma  mark - TableViewDelegate
    
    
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if ([self isDirectoryAtIndexPath:indexPath])
        {
            NSString * fileName = [self.contents objectAtIndex:indexPath.row];
            NSString * path = [self.path stringByAppendingPathComponent:fileName];
            // 1 вариант - инициализцая класса через создание нового класса
            //ASDirectoryViewController * vc = [[ASDirectoryViewController alloc] initWitthFolderPath:path];
            //[self.navigationController pushViewController:vc animated:YES];
            
            //>>>>>>>>>>>>>>>>>>>> 2 варианит - инициализация класса через storyboard
            //        // пример взятия storyborda
            //        //        UIStoryboard * storyboard = self.storyboard;
            //
            //        //пример взятия по имени
            //        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //        ASDirectoryViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"ASDirectoryViewController"];
            //        vc.path = path;
            //        [self.navigationController pushViewController:vc animated:YES];
            
            // <<<<<<<<<<<<<<<<<<
            
            
            
            
            // >>>>>>>>>>>>>>>>>>>>> 3 вариант - через идентифаер Сигвея в storyboard
            self.selectedPath = path;
            [self performSegueWithIdentifier:@"navigateDeep" sender:nil];
            
            // <<<<<<<<<<<<<<<<<<<<<
        }
    }
    
    //для того, чтоб разрешить изменение высоты ячейки
    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        if ([self isDirectoryAtIndexPath:indexPath])
        {
            return 44.f;
        }
        else
        {
            return 80.f;
        }
    }
    
#pragma mark  - Seque
    - (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
        NSLog(@"shouldPerformSegueWithIdentifier: %@", identifier);
        return YES;
    }
    
    - (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        NSLog(@"prepareForSegue: %@", segue.identifier);
        
        ASDirectoryViewController * vc = segue.destinationViewController;
        vc.path = self.selectedPath;
    }
 
 */
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 35 - TableView search (NSOperation theard!!!!)
    
    /*
#import "ViewController.h"
#import "NSString+Random.h"
#import "Section.h"
#import "MyTask.h"
    
    @interface ViewController () <UISearchBarDelegate>
    
    @property (strong, nonatomic) NSArray * namesArray;
    @property (strong, nonatomic) NSArray * sectionsArray;
    
    
    @property (strong, nonatomic) NSOperationQueue * operationsQueue;
    
    @property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        self.operationsQueue = [[NSOperationQueue alloc] init];
        
        NSMutableArray * array  =[NSMutableArray array];
        for (NSInteger i = 0; i < 2000000; i++) {
            [array addObject:[[NSString randomAlphanumericString] capitalizedString]];
        }
        
        NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
        [array sortUsingDescriptors:@[sortDescriptor]];
        
        self.namesArray = array;
        
        // создание категорий секций на главном потоке
        //    self.sectionsArray = [self generateSectionsFromArray:self.namesArray withFilter: self.searchBar.text] ;
        //    [self.tableView reloadData];
        
        // создание категорий секций на бэкграунд потоке (оптимизация)
        [self generateSectionsInBackgroundFromArray: self.namesArray withFilter:self.searchBar.text];
        
    }
    
    - (void) generateSectionsInBackgroundFromArray: (NSArray *) array withFilter: (NSString *) filterString {
        
        __weak ViewController * weakSelf = self;
        
        [self.operationsQueue cancelAllOperations];
        self.operationsQueue = [[NSOperationQueue alloc] init];
        
        NSLog(@"current number operations - %li", [self.operationsQueue operationCount]);
        
        MyTask * task = [[MyTask alloc] initWithBlock:^{
            
            NSArray * sectionsArray = [weakSelf generateSectionsFromArray:array withFilter:filterString];
            
            // операция на главном потоке!!
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (sectionsArray)
                    weakSelf.sectionsArray = sectionsArray;
                [weakSelf.tableView reloadData];
            });
        }];
        
        
        [self.operationsQueue addOperation:task];
        
        
        
        
        
    }
    
    - (NSArray *) generateSectionsFromArray: (NSArray *) array withFilter: (NSString *) filterSstring{
        NSMutableArray * sectionsArray = [NSMutableArray array];
        
        NSString * currentLetter = nil;
        for (NSString * string in self.namesArray) {
            
            if ([filterSstring length] > 0 && [string rangeOfString:filterSstring].location == NSNotFound)
            {
                continue;
            }
            
            NSString * firstLetter = [string substringToIndex:1];
            Section * section = nil;
            if (![currentLetter isEqualToString: firstLetter]) {
                section = [[Section alloc] init];
                section.sectionName = firstLetter;
                section.itemsArray = [NSMutableArray array];
                currentLetter = firstLetter;
                [sectionsArray addObject:section];
            }
            else {
                section = [sectionsArray lastObject];
            }
            
            [section.itemsArray addObject:string];
        }
        return sectionsArray;
    }
    
#pragma mark - UITableViewDataSource
    
    
    - (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
        NSMutableArray * array = [NSMutableArray array];
        
        for (Section * section in self.sectionsArray) {
            [array addObject:section.sectionName];
        }
        return array;
    }
    
    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return self.sectionsArray.count;
        
    }
    
    - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
        return [[self.sectionsArray objectAtIndex:section] sectionName];
    }
    
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        Section * sec = [self.sectionsArray objectAtIndex:section];
        return [sec.itemsArray count];
    }
    
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString * identifier = @"Cell";
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        Section * section = [self.sectionsArray objectAtIndex:indexPath.section];
        NSString * name = [section.itemsArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = name;
        return cell;
    }
#pragma mark - UISearchBarDelegate
    
    - (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
        [searchBar setShowsCancelButton:YES animated:YES];
    }
    
    - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
        [searchBar resignFirstResponder];
        [searchBar setShowsCancelButton:NO animated:YES];
        
    }
    
    - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
        // создание категорий секций на главном потоке
        //    self.sectionsArray = [self generateSectionsFromArray:self.namesArray withFilter: self.searchBar.text] ;
        //    [self.tableView reloadData];
        
        // создание категорий секций на бэкграунд потоке (оптимизация)
        [self generateSectionsInBackgroundFromArray: self.namesArray withFilter:self.searchBar.text];
        
    }

     */
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 36 - UIPopover

    /*
    
#import "ViewController.h"
#import "ASDetailsViewController.h"
    
    @interface ViewController () <UIViewControllerTransitioningDelegate, UIPopoverPresentationControllerDelegate>
    
    @property (strong, nonatomic) ASDetailsViewController * vc;
    
    
    
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
    }
    
#pragma mark - Actions
    
    - (IBAction)actionAdd:(UIBarButtonItem *)sender {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {  // для iPad только можно использовать поповеры
            ASDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
            self.vc = vc;
            vc.transitioningDelegate = self;
            
            // выбор стиля презентации вью при методе presentViewController
            vc.modalPresentationStyle = UIModalPresentationPopover;
            
            //создаем поповера с контролера, для его настройки
            UIPopoverPresentationController  * popover = vc.popoverPresentationController;
            
            // bound источника от которого будет всплывать
            
            
            // вытянуть view с sender (UIBarButtonItem - не является наследником UIViewController
            UIView * sourceView = [sender valueForKey:@"view"];
            CGRect sourceRect;
            if(sourceView){
                sourceRect = [sourceView frame];
                // назначение вью при нажатии на который будет всплывать поповер
                popover.sourceView = sourceView;
            }
            else{
                sourceRect = CGRectMake(0, 0, 0, 0);
            }
            popover.sourceRect = sourceRect;
            
            //предпочительное направление появления popover
            popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
            
            
            // подписка на делегат, чтоб при выходе удалялся поповер вместе в вью
            popover.delegate = self;
            
            
            
            // рекомендуемы размер вью popoverа
            vc.preferredContentSize = CGSizeMake(100, 100);
            
            [self presentViewController:vc animated:YES completion:^{
                NSLog(@"popover arrow = %li", popover.arrowDirection);
            }];
            
            
            
            // закрытие вью через 3 сек
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:^{
                    self.vc = nil;
                }];
            });
        }
        
    }
    
    - (IBAction)actionPressMe:(UIButton *)sender {
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {  // для iPad только можно использовать поповеры
            ASDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
            self.vc = vc;
            vc.transitioningDelegate = self;
            
            
            // рекомендуемый размер вью popoverа
            vc.preferredContentSize = CGSizeMake(100, 100);
            
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            
            
            // выбор стиля презентации вью при методе presentViewController
            vc.modalPresentationStyle = UIModalPresentationPopover;
            
            //создаем поповера с контролера, для его настройки без navigationcontroller
            //UIPopoverPresentationController  * popover = vc.popoverPresentationController;
            
            //создаем поповера с контролера, для его настройки c navigationcontroller
            UIPopoverPresentationController  * popover = nav.popoverPresentationController;
            
            
            
            
            /// назначение вью при нажатии на который будет всплывать поповер
            popover.sourceView = sender;
            // bound источника от которого будет всплывать
            popover.sourceRect = sender.bounds;
            //предпочительное направление появления popover
            popover.permittedArrowDirections = UIPopoverArrowDirectionRight;
            
            
            
            
            // подписка на делегат, чтоб при выходе удалялся поповер вместе в вью
            popover.delegate = self;
            
            
            
            
            
            
            [self presentViewController:nav animated:YES completion:^{
                NSLog(@"popover arrow = %li", popover.arrowDirection);
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:^{
                    self.vc = nil;
                }];
            });
            
        }
        
    }
    
#pragma mark -  UIPopoverPresentationControllerDelegate
    
    - (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
        // чтоб удалился вью котролер после того как уйдет view
        self.vc = nil;
    }
    
    
#pragma mark - Segue
    
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        
    }
    
    
    @end
     */
    
    
    
 
    
    
    
    
    
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 37 - 38 - MKMapView
    
    /*
#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "ASMapAnnotation.h"
#import "UIView+MKAnnotationView.h"
    
    
    
    
    @interface ViewController () <MKMapViewDelegate>
    
    @property (nonatomic, strong) CLGeocoder * geoCoder;
    @property (strong, nonatomic) MKDirections * directions;
    
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        
        
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self
                                                                                    action:@selector(actionAdd:)];
        
        // кнопка для того чтобы между кнопками было расстояние (фиксированная ширина) - fixed кнопка. Flexible button - делает отступ максимальное между кнопками
        UIBarButtonItem * fixedButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil
                                                                                      action:nil];
        fixedButton.width = 50;
        
        UIBarButtonItem * zoomButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self
                                                                                     action:@selector(avtionShowAll:)];
        self.navigationItem.rightBarButtonItems = @[zoomButton, fixedButton, addButton];
        
        
        self.geoCoder = [[CLGeocoder alloc] init];
        
    }
    
    - (void)dealloc
    {
        if ([self.geoCoder isGeocoding])
            [self.geoCoder cancelGeocode];
        if ([self.directions isCalculating])
            [self.directions cancel];
    }
    
#pragma mark - Private Methods
    
    - (void)alertWithTitle: (NSString *) title andMessege: (NSString *) message {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title
                                                                                  message:message
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:okButton];
        [self presentViewController:alertController animated:YES completion: nil ];
    }
    
#pragma mark - Actions
    
    
    
    - (void) actionDirection: (UIButton *) sender {
        
        
        // direction для прокладки пути от первого пина в массиве до второго пина
        if (self.mapView.annotations.count == 2)
        {
            
            // локейшн пина
            CLLocationCoordinate2D coordinateSourcePin = [self.mapView.annotations objectAtIndex:0].coordinate;
            MKPlacemark * placemark = [[MKPlacemark alloc]initWithCoordinate:coordinateSourcePin];
            MKMapItem * source = [[MKMapItem alloc] initWithPlacemark:placemark];
            
            CLLocationCoordinate2D coordinateTargetPin = [self.mapView.annotations objectAtIndex:1].coordinate;
            MKPlacemark * placemarkTargetPin = [[MKPlacemark alloc]initWithCoordinate:coordinateTargetPin];
            MKMapItem * destinationTargetPin = [[MKMapItem alloc] initWithPlacemark:placemarkTargetPin];
            
            
            
            MKDirectionsRequest * request = [[MKDirectionsRequest alloc] init];
            request.source = source;
            request.destination = destinationTargetPin;
            
            request.transportType = MKDirectionsTransportTypeAutomobile;
            request.requestsAlternateRoutes = YES;
            
            self.directions = [[MKDirections alloc] initWithRequest:request];
            [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
                
                if (error) {
                    [self alertWithTitle:@"Error" andMessege:[error localizedDescription]];
                }
                else if ([response.routes count] == 0)
                    [self alertWithTitle:@"Error" andMessege:@"No routes found"];
                else {
                    [self.mapView removeOverlays:self.mapView.overlays];
                    
                    NSMutableArray * array = [NSMutableArray array];
                    for (MKRoute * route  in response.routes) {
                        [array addObject: route.polyline];
                    }
                    
                    [self.mapView addOverlays:array level:MKOverlayLevelAboveRoads];
                    
                }
                
                
                
                
            }];
        }
        
        // direction для прокладки пути от текущего местоположения к пину где вызывается метод
        //
        //        // метод из расширения категории вью
        //        MKAnnotationView * annotationView = [sender superAnnotationView];
        //
        //        if (!annotationView) {
        //            return;
        //        }
        //
        //        if ([self.directions isCalculating])
        //            [self.directions cancel];
        //
        //        // локейшн пина на чью кнопочку мы нажали
        //        CLLocationCoordinate2D coordinate = annotationView.annotation.coordinate;
        //
        //
        //
        //
        //        MKDirectionsRequest * request = [[MKDirectionsRequest alloc] init];
        //        request.source = [MKMapItem mapItemForCurrentLocation];
        //
        //        MKPlacemark * placemark = [[MKPlacemark alloc]initWithCoordinate:coordinate];
        //
        //        MKMapItem * destination = [[MKMapItem alloc] initWithPlacemark:placemark];
        //
        //        request.destination = destination;
        //        request.transportType = MKDirectionsTransportTypeAutomobile;
        //
        //        self.directions = [[MKDirections alloc] initWithRequest:request];
        //        [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
        //
        //            if (error) {
        //                [self alertWithTitle:@"Error" andMessege:[error localizedDescription]];
        //            }
        //            else if ([response.routes count] == 0)
        //                [self alertWithTitle:@"Error" andMessege:@"No routes found"];
        //            else {
        //                [self.mapView removeOverlays:self.mapView.overlays];
        //
        //                NSMutableArray * array = [NSMutableArray array];
        //                for (MKRoute * route  in response.routes) {
        //                    [array addObject: route.polyline];
        //                }
        //
        //                [self.mapView addOverlay:array level:MKOverlayLevelAboveRoads];
        //
        //            }
        //
        //
        //
        //
        //        }];
        //
        
        
    }
    
    //кнопка которая посажена на пины
    - (void)actionDescription: (UIButton *) sender {
        // метод из расширения категории вью
        MKAnnotationView * annotationView = [sender superAnnotationView];
        
        if (!annotationView) {
            return;
        }
        
        
        // колейшн пина на чью кнопочку мы нажали
        CLLocationCoordinate2D coordinate = annotationView.annotation.coordinate;
        
        // ищем адрес по локации
        CLLocation * location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        
        if ([self.geoCoder isGeocoding])
            [self.geoCoder cancelGeocode];
        
        [self.geoCoder reverseGeocodeLocation:location
                            completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                
                                NSString * message = nil;
                                if (error) {
                                    message = [error localizedDescription];
                                } else {
                                    if ([placemarks count] > 0) {
                                        CLPlacemark * placeMark = [placemarks firstObject];
                                        message = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n",
                                                   placeMark.name,
                                                   placeMark.thoroughfare,
                                                   placeMark.subThoroughfare,
                                                   placeMark.locality,
                                                   placeMark.subLocality,
                                                   placeMark.administrativeArea,
                                                   placeMark.subAdministrativeArea,
                                                   placeMark.postalCode,
                                                   placeMark.ISOcountryCode,
                                                   placeMark.country,
                                                   placeMark.inlandWater,
                                                   placeMark.ocean,
                                                   placeMark.areasOfInterest];
                                    }
                                    else {
                                        message = @"No Placemarks Found";
                                    }
                                }
                                
                                
                                [self alertWithTitle:@"Location" andMessege:message];
                                
                            }];
    }
    
    - (void) avtionShowAll: (UIBarButtonItem *) sender {
        MKMapRect zoomRect = MKMapRectNull;
        
        for (id <MKAnnotation> annotation in self.mapView.annotations) {
            CLLocationCoordinate2D location = annotation.coordinate;
            MKMapPoint center = MKMapPointForCoordinate(location);
            static double delta = 20000;
            MKMapRect rect = MKMapRectMake (center.x - delta, center.y - delta, delta * 2, delta * 2);
            // обьеденение rect (с двух прямогульников сделать один общий)
            zoomRect = MKMapRectUnion(zoomRect, rect);
        }
        // функция объеденения (на всякий случай) - может быть узкий или широкий, а эта функция подпровляет прямоугольник, который будет помещаться на экране
        zoomRect = [self.mapView mapRectThatFits:zoomRect];
        
        // вывод прямоугльника на экран. edgePadding - отступ от края экрана
        [self.mapView setVisibleMapRect:zoomRect edgePadding: UIEdgeInsetsMake(50, 50, 50, 50) animated:YES];
    }
    - (void) actionAdd: (UIBarButtonItem *) sender {
        
        ASMapAnnotation * annotation = [[ASMapAnnotation alloc] init];
        annotation.title = @"Test title";
        annotation.subtitle = @"Test Subtitle";
        annotation.coordinate = self.mapView.region.center;
        
        
        [self.mapView addAnnotation:annotation];
    }
    
#pragma  mark - MKMapViewDelegate
    // используется для вырисовки маршрута на карте
    - (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
        //if ([overlay isKindOfClass:[MKRoutePo class]]) {
        MKPolylineRenderer * renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        renderer.lineWidth = 6.f;
        renderer.strokeColor = [UIColor redColor];
        return renderer;
        // }
        return nil;
    }
    
    //- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    //    NSLog(@"regionWillChangeAnimated");
    //}
    //
    //- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    //    NSLog(@"regionDidChangeAnimated");
    //}
    //
    //- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView {
    //    NSLog(@"mapViewWillStartLoadingMap");
    //}
    //
    //- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView {
    //    NSLog(@"mapViewDidFinishLoadingMap");
    //}
    //
    //- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error {
    //    NSLog(@"mapViewDidFailLoadingMap");
    //}
    //
    //- (void)mapViewWillStartRenderingMap:(MKMapView *)mapView {
    //    NSLog(@"mapViewWillStartRenderingMap");
    //}
    //
    //- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered {
    //    NSLog(@"mapViewDidFinishRenderingMap");
    //}
    
    
    - (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
        if ([annotation isKindOfClass:[MKUserLocation class]]) {
            return nil;
        }
        
        static NSString * identifier = @"Annotation";
        MKPinAnnotationView * pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (!pin)
        {
            pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            pin.pinTintColor = [UIColor purpleColor];
            pin.animatesDrop = YES;
            pin.canShowCallout = YES;
            pin.draggable = YES;
            
            // добавление кнопки
            UIButton * descriptionButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [descriptionButton addTarget:self action:@selector(actionDescription:) forControlEvents:UIControlEventTouchUpInside];
            // поставить кнопку на всплывающее вью справа от пин
            pin.rightCalloutAccessoryView = descriptionButton;
            
            
            // добавление кнопки
            UIButton * directionButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
            [directionButton addTarget:self action:@selector(actionDirection:) forControlEvents:UIControlEventTouchUpInside];
            // поставить кнопку на всплывающее вью справа от пин
            pin.leftCalloutAccessoryView = directionButton;
        }
        else {
            pin.annotation = annotation;
        }
        return pin;
    }
    
    
    - (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
        
        if (newState == MKAnnotationViewDragStateEnding) {
            CLLocationCoordinate2D location = view.annotation.coordinate;
            MKMapPoint poin = MKMapPointForCoordinate(location);
            NSLog(@"location - {%f, %f}\noint = %@", location.latitude, location.longitude, MKStringFromMapPoint(poin) );
        }
        
    }
     
     */

    
    
    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 39 - WKWebView
    
/*
#import "ViewController.h"
    
    @interface ViewController () <WKNavigationDelegate, WKUIDelegate>
    @property (weak, nonatomic) IBOutlet UIBarButtonItem *backItem;
    @property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardItem;
    
    @property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        self.webView.navigationDelegate = self;
        [self.activityIndicator setHidesWhenStopped:YES];
        
        
        
        NSString * urlString = @"https://www.iphones.ru";
        NSURL * url = [NSURL URLWithString:urlString];
        NSURLRequest * requeste = [NSURLRequest requestWithURL: url];
        [self.webView loadRequest: requeste];
        
        
        // путь к файлу который находится в папке приложения
        NSString * filePath = [[NSBundle mainBundle] pathForResource:@"pdf.pdf" ofType:nil];
//         загрузка pdf файла локального
//         // загрузка файла через мэйн бандл - пдф файла
//
//         NSData * fileData = [NSData dataWithContentsOfFile:filePath];
//
//         if (fileData)
//         [self.webView loadData: fileData
//         MIMEType:@"application/pdf"      // указывается для того, чтоб веб знал что за файл он загружает
//         characterEncodingName: @"UTF-8"           // кодировка
//         baseURL:nil];
        
        
         загрузка pdf файла локального
//         NSURL * fileUrl = [NSURL fileURLWithPath:filePath];
//         NSURLRequest * request = [NSURLRequest requestWithURL:fileUrl];
//         [self.webView loadRequest:request];
        
        
        // кросс платформенный метод
        //    NSString * htmlString = @"<html>"
        //                                "<body>"
        //                                    "<p style=\"text-align: center; font-size: 100\">Hello World!  </p>"
        //    "<a href=\"cmd:show_alert\">TEST BUTTON</a>"
        //                                "</body>"
        //                             "</html>";
        //    [self.webView loadHTMLString:htmlString baseURL:nil];
    }
    
#pragma mark - Privet Methods
    
    - (void)alertWithTitle: (NSString *) title andMessege: (NSString *) message {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title
                                                                                  message:message
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:okButton];
        [self presentViewController:alertController animated:YES completion: nil ];
    }
    
    
#pragma mark - WKNavigationDelegate
    
    - (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
        
        NSLog(@"didStartProvisionalNavigation %@", navigation);
        [self.activityIndicator startAnimating];
        
    }
    
    - (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
        
        NSLog(@"didCommitNavigation %@", navigation);
    }
    
    - (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
        
        NSLog(@"didFinishNavigation %@", navigation);
        
        
        
        
        [self.activityIndicator stopAnimating];
        
        //    self.backItem.enabled = [self.webView canGoBack];
        //    self.forwardItem.enabled = [self.webView canGoForward];
        
    }
    
    - (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
        
        NSString * urlString = navigationAction.request.URL.absoluteString;
        
        
        if ([urlString isEqualToString:@"cmd:show_alert"])
        {
            [self alertWithTitle:@"TEST BUTTON ALERT" andMessege:@"You are head"];
            decisionHandler (WKNavigationActionPolicyCancel);
        }
        else
            decisionHandler(WKNavigationActionPolicyAllow);
        
    }
    
#pragma mark - Actions
    - (IBAction)actionBack:(UIBarButtonItem *)sender {
        if ([self.webView canGoBack])
            [self.webView goBack];
    }
    
    
    - (IBAction)actionForward:(UIBarButtonItem *)sender {
        if ([self.webView canGoForward])
            [self.webView goForward];
    }
    
    - (IBAction)actionRefrash:(UIBarButtonItem *)sender {
        [self.webView stopLoading];
        [self.webView reload];
    }
    
 */

    
    
    
    
    
    
    
// ***********************************************************************************************************************
#pragma mark - Lesson 48 - Tricks
    

    
// ASUtils.h
    /*

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
    
#ifndef UTILS
#define UTILS
    
    
#define PRODUCTION_BUILD
    
#define APP_SHORT_NAME @"Tricks"
    
    //#define LOGS_ENABLED 1   // дефайн которая используется в ASLog - при 1 выводи ASLog в консольк при 0 не выводит
    
#define LOGS_NOTFOCATIONS_ENABLED 1
    
    extern NSString * const ASLogNotification;
    extern NSString * const ASLogNotificationTextUserInfoKey;
    
    
    // создание дефайна для создания цвета. В коде RGBA(r,g,b,a) будет заменено на выражение с права, с подставленным параметрами
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f]
    
    
    NSString * fancyDateStringFromDate (NSDate * date);
    
    BOOL iPad(void);
    BOOL iPhone(void);
    
    typedef enum {
        ASProgrammerTypeJunior,
        ASProgrammerTypeMid,
        ASProgrammerTypeSenior
    } ASProgrammerType;
    
    NSString * NSStringFromASProgrammerType(ASProgrammerType type);
    
    
    void ASLog(NSString * format, ...);
    
    
#endif
     */
    
    
    
    
    
    
    
// ASUtils.m
/*
#import "ASUtils.h"
    
    NSString * const ASLogNotification = @"com.askutarenko.ASLogNotification";
    NSString * const ASLogNotificationTextUserInfoKey = @"com.askutarenko.ASLogNotificationTextUserInfoKey";
    
    NSString * fancyDateStringFromDate (NSDate * date) {
        static NSDateFormatter * formatter = nil;
        
        if (!formatter) {
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"-- dd : MM : yy --"];
        }
        
        
        
        return [formatter stringFromDate:date];
    }
    
    
    BOOL iPad(void) {
        return ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    }
    
    BOOL iPhone(void) {
        return ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
    }
    
    NSString * NSStringFromASProgrammerType(ASProgrammerType type) {
        switch (type) {
            case ASProgrammerTypeJunior:    return @"ASProgrammerTypeJunior";
            case ASProgrammerTypeMid:       return @"ASProgrammerTypeMid";
            case ASProgrammerTypeSenior:    return @"ASProgrammerTypeSenior";
                
            default: return  nil;
        }
    }
    
    
    void ASLog(NSString * format, ...) {
        
#if LOGS_ENABLED            // выводить или не выводить в консоль текст
        
        va_list argumentList;
        va_start (argumentList, format);
        NSLogv(format, argumentList);
        
        
#if LOGS_NOTFOCATIONS_ENABLED  // создание нотификации, для отслеживания ASLoga и вывода их в консоль (textView) приложения
        NSString * log = [[NSString alloc] initWithFormat:format arguments:argumentList];
        [[NSNotificationCenter defaultCenter] postNotificationName:ASLogNotification
                                                            object:nil
                                                          userInfo:@{ASLogNotificationTextUserInfoKey: log}];
#endif
        va_end(argumentList);
        
#endif
    }
 */
    
    
    
    
    
// ViewController.h
/*
#import "ViewController.h"
#import "ASUtils.h"
    
    @interface ViewController ()
    @property (weak, nonatomic) IBOutlet UITextView *consoleView;
    
    @end
    
    @implementation ViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:ASLogNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification * _Nonnull note) {
                                                          
                                                          self.consoleView.text = [NSString stringWithFormat:@"%@\n%@", self.consoleView.text, [note.userInfo objectForKey:ASLogNotificationTextUserInfoKey] ];
                                                      }];
        
#ifdef PRODUCTION_BUILD
        ASLog(@"%@", fancyDateStringFromDate([NSDate date])) ;
        
        if (iPad()) {
            ASLog(@"iPad");
        }
        
        if (iPhone()) {
            ASLog(@"iPhone");
        }
        
        ASLog(@"%@", NSStringFromASProgrammerType(ASProgrammerTypeSenior));
        
        ASLog(@"%@", APP_SHORT_NAME);
#endif
        
        
        self.view.backgroundColor = RGBA(200, 130, 180, 100);
        
        
        
        
    }
    
    - (void)dealloc {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    
    - (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
    }
    
    - (IBAction)actionTest:(UIButton *)sender {
        ASLog(@"actionTest");
    }
    
    @end
*/

    
    
    
    
    
    
    
    
    







    
        
        
        
#pragma mark - NSException (Bonus)
    /*
    @try  //пробовать
    {
        //любой код
    }
    @catch (NSException * excep)  //пойматьN
    {
        NSLog(@"%@", excep);//обработка предоупреждения
    }
    //следуещее не обязатлеьно
    @finally //наконец
    {
        //код который выполнится при любых условиях
    }
    
    //любая функция
    - (int) division: (int) a and: (int) b
    {
        if (b == 0)
            @throw [NSException exceptionWithName:@"DivideByZero" reason:@"Attempt to divide by zero!" userInfo:nil] //throw - бросать
            return a/b;
        
    }
     */
    
    
  
 
// ####################################### NOTES
  #pragma mark - NOTES

    /*
     
     UI_APPEARANCE_SELECTOR -  стоит возле property. Дальше в курсе скутаренко расскажет зачем
     
    ************************************************************************************************************************
    // работа с ориентированием. animateAlongsideTransition - эквивалент didRotateFromInterfaceOrientation
    - (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
    {
        [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
         {
             UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
             // do whatever
         } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
         {
             
         }];
        
        [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    }
     ************************************************************************************************************************
    
 self.myUITextField.clearButtonMode = UITextFieldViewModeAlways; // добавление кнопки очистки поля в TextField
 
 Куча (память) - heap
 стек (память) - stack
 
 [obj isKindOfClass:[ASChild class]] - выясняет является ли obj классом ASChild или любого класса наследника.
 - (BOOL)isKindOfClass:(Class)aClass;
 
 
 arc4random() - генерация 255 битного числа
 
 #pragma mark - ASPatient - переход на методы (вверху в строке) которые указаные ниже данного метода
 
 */
    
#pragma mark - Angle of circle
/*
 Формула для перевода градусов в Пи
    angleInPi = angle * M_PI / 180;
 
 В окружности:
 - верх (0 градусов) -      2 * PI    (или 0)
 - право (90 градусов) -    PI/2
 - низ (180 градусов) -     PI
 лево (270 градусов) -      3*PI/2
 
*/
    
        
        
        
    
    return 0;
}



 
 
 
 
 
 
 
 
 
 
 
 
 
