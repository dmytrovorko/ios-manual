///*
// Алгоритм работы с файлами
// 1. Откраытие файла
// FILE* fopen (const char*name, const char * mode);
// mode - режим работы с файлом - открывает файл в нужном режиме и возраващает указатель на него.
// Режимы работы с файлами:
// 1. Текстовые режимы работы с файлами,
// - "r" - открывает файл для чтение. Если его нет возвращает NULL.
// - "w" - whrite. Открывает файл для записи (не возможно чтение). Если файла нет, он будет создан. Если он есть содержимое его будет стерто.
// - "a" - append. Открывает файл для записи в конец. Если файла нет, он будет создан.
// - "w+" "r+" "a+" - возможно в каждом режиме и чтение и запись. Но есть свои особенности. В режиме r+ - можено открыть файл., не можете создать новый, но записывает поверх содержимого (т.к. указатель ставится в начало файла). В режиме w+ - так же само стирается существующий или создается новый. Прочесть мы сможем прочитать только то, что сами написали. Режим a+ - может создать новый файл, поддерживает запись в конец файла, чтение с начала файла, создание нового файла.
// 2. Бинарные режими работы с файламиы:
// - "rb" "wb" "ab+" "rb" "wb+" "ab+" - разница, что работают бинарно.
//
// 2. Чтение / Запись:
// существуют несколько режимов - текстовый, строчный, символьный, блочный режимы.
//
//
// 3. Закрыти файла.
// int fclose(FILE*) - закрывает файл по переданному указателю. Возвращает интовое значение - код успешного закрытия.
//
//
// Символьный режим -
// int fgetc (FILE*); - возвращает код прочитанного символа. Автоматически сдвигает указатель. По достежению конца файла возвращает EOF (-1).
// int fputc (int, FILE*); - записывает в файл символ, код которого переданн первым параметром. Возвращает код записываемого символа.
//
// Построчный режим:
// char* fgets (char *dest, int count, FILE * file) - читает текущую строку из файла. Возвращает прочитанную строку. По достижению конца файла возращает константу NULL. Dest - массив, куда будет записана прочитаная строка. count - максимальное количство символов, которое строке надо прочесть. file - файловый  указатель с которого читают.
// int fputs (const char *source, FILE*file) - положить строку в файл. Сохраняет в файл в строку переданный первым параметром. Возвращает количетсво записанных символов.
//
// Текстовый режим чтения/записи
// int fprintf (FILE *file, const char *format, ...); - записывает в файл данные в заданном формате.
// int fscanf (FILE *file, const char *format, ...); - считывает из файла данные в указанном формате.
//
//
//
//
//
// void rewind (FILE *) - взвращает указатель в начало файла
// int feof (FILE *file) - возвращает еденицу если достигнут конец файла
//
// */
//
//
//#import <Foundation/Foundation.h>
//#include <stdlib.h>
//#include <string.h>
//
//enum Menu {
//    encoded = 1,
//    decoded,
//    decodedWithoutKey,
//    ex
//};
//
//char encodedCh (char ch, int key);
//char decodedCh (char ch, int key);
//double freqLett (int key, FILE *f);
//
//int main(int argc, const char * argv[]) {
//    char ch;
//    int key = 5,
//    iter = 0,
//    keyDec = 0;
//    char fileN [50], fileN2 [50];
//    double deviation = 0,
//    buf = 0;
//    FILE * f1 = NULL, * f2 = NULL;
//    
//    enum Menu choise;
//
//    while (1)
//    {
//        printf("Menu:\n1 - Encoded file.\n2 - Decoded file by Key \n3 - Decoded file without Key\n4 - Exit\n");
//        scanf("%u", &choise);
//        switch (choise) {
//            case encoded:
//                {
//                    if(argc > 1 && iter == 0)
//                    {
//                        strcpy(fileN, argv[1]);
//                        iter++;
//                    }
//                    else{
//                        printf("Input name file for encoder: ");
//                        scanf("%s", fileN);
//                    }
//                    strcpy(fileN2, "encode_");
//                    strcat(fileN2, fileN);
//                    f1 = fopen (fileN, "r");
//                    if (f1 == NULL){
//                        printf("Error opening file %s\n", fileN);
//                        break;
//                    }
//                    f2 = fopen (fileN2, "w");
//                    if ( f2 == NULL){
//                        printf("Error opening file\n");
//                        break;
//                    }
//                    printf("Input key of encoded (1 - 26): ");
//                    scanf("%i", &key);
//                    while (key < 1 || key > 26)
//                    {
//                        printf("Pleas input key in the interval 1 - 26: ");
//                        scanf("%i", &key);
//                    }
//                    while ((ch = fgetc(f1)) != EOF)
//                        fputc(encodedCh(ch, key), f2);
//
//                    if (f2 != NULL) fclose(f2);
//                    if (f1 != NULL) fclose(f1);
//                    system("clear");
//                    printf("\n\n\t***File was Encoded***\n\n");
//
//                }
//                break;
//            case decoded:
//                {
//                    if(argc > 1 && iter == 0)
//                    {
//                        strcpy(fileN, argv[1]);
//                        iter++;
//                    }
//                    else{
//                        printf("Input name file for decoded: ");
//                        scanf("%s", fileN);
//                    }
//
//                    strcpy(fileN2, "decoded_");
//                    strcat(fileN2, fileN);
//
//                    f1 = fopen (fileN, "r");
//                    if (f1 == NULL){
//                        printf("Error opening file %s\n", fileN);
//                        break;
//                    }
//                    f2 = fopen (fileN2, "w");
//                    if ( f2 == NULL){
//                        printf("Error opening file\n");
//                        break;
//                    }
//                    printf("Input key of decoded (1 - 26): ");
//                    scanf("%i", &key);
//
//                    while (key < 1 || key > 26)
//                    {
//                        printf("Pleas input key in the interval 1 - 26:\n");
//                        scanf("%i", &key);
//                    }
//
//                    while ((ch = fgetc(f1)) != EOF)
//                        fputc(decodedCh(ch, key), f2);
//
//                    if (f2 != NULL) fclose(f2);
//                    if (f1 != NULL) fclose(f1);
//                    system("clear");
//                    printf("\n\n\t***File was Decoded***\n\n");
//
//                }
//                break;
//            case decodedWithoutKey:
//            {
//                if(argc > 1 && iter == 0)
//                {
//                    strcpy(fileN, argv[1]);
//                    iter++;
//                }
//                else{
//                    printf("Input name file for decoded without Key: ");
//                    scanf("%s", fileN);
//                }
//
//                strcpy(fileN2, "decodedWithoutKey_");
//                strcat(fileN2, fileN);
//
//                f1 = fopen (fileN, "r");
//                if (f1 == NULL){
//                    printf("Error opening file %s\n", fileN);
//                    break;
//                }
//                f2 = fopen (fileN2, "w");
//                if ( f2 == NULL){
//                    printf("Error opening file\n");
//                    break;
//                }
//
//                
//                
//                
//                
//                for (key = 1; key <= 26; key++)
//                {
//                    buf = freqLett(key, f1);
//                    if (key == 1){
//                        deviation = buf;
//                        keyDec = key;
//                    }
//                    else if (buf < deviation){
//                        deviation = buf;
//                        keyDec = key;
//                    }
//                }
//                
//                while ((ch = fgetc(f1)) != EOF)
//                    fputc(decodedCh(ch, keyDec), f2);
//
//                if (f2 != NULL) fclose(f2);
//                if (f1 != NULL) fclose(f1);
//                system("clear");
//                printf("\n\n\t*** Big chance that key = %i ***\n\n", keyDec);
//            }
//                break;
//            case ex:{
//                        printf("\nProgram has Ended\n");
//                        return 0;
//                }
//            default:
//            {
//                printf("Not Correct input\n");
//            }
//                break;
//        }
//    }
//
//
//
//
//    return 0;
//}
//
//double freqLett (int key, FILE *fl)
//{
//    double deviation = 0;
//    const double NORM_COUNT = 71.399;
//    char ch;
//    int litFreq = 0;
//    int count = 0;
//
//    while((ch = tolower (fgetc(fl))) != EOF) {
//        if(ch >= 'a' && ch <= 'z')
//        {
//            ch = decodedCh(ch, key);
//            if (ch == 'a' || ch == 't' || ch == 'e' || ch == 'o' || ch == 'n' || ch == 'r' || ch == 'i' || ch == 's' || ch == 'h')
//                litFreq++;
//            count ++;
//        }
//    }
//    deviation = (((double)litFreq * 100) / (double)count) - NORM_COUNT;
//    rewind(fl);
//    return fabs (deviation);
//}
//
//char decodedCh (char ch, int key)
//{
//    if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
//    {
//        if((ch - key) < 'A' && ch <= 'Z')
//            return ('Z' - (key - (ch - 'A') - 1));
//
//        else if ((ch - key) < 'a' && ch >= 'a')
//            return(('z' - (key - (ch - 'a') - 1)));
//
//        else return (ch - key);
//    }
//    else return ch;
//}
//
//char encodedCh (char ch, int key){
//    if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
//    {
//        if((ch + key) >'Z' && ch <= 'Z')
//            return ('A' + (key - ('Z' - ch) - 1));
//
//        else if ((ch + key) > 'z' && ch <= 'z')
//            return(('a' + (key - ('z' - ch) - 1)));
//
//        else return (ch + key);
//    }
//    else return ch;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
