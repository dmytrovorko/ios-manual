/*
 
 Функции с переменным количеством аргументов
 
 1.Функция должна иметь как минимум один явный параметр.
 2. Список параметров должен заканчиваться троеточиями
 3. Для работы со списком используем переменная тип va_list имя;
 4. Эту переменную необходимо проинецеализировать с использованием макроса va_start (имя списка, имя переменной предшествующей троеточию);
 5. Извлечь очередной аргумент из списка можно при помощи макроса va_arg (имя списка, тип данных который нужно извлечь);
 6. В конце роботы необходимо очистить список при помощи макроса va_end(имя списка);
 
 */

//#import <Foundation/Foundation.h>
//double summa(double, ...);
//
//int main (void)
//{
//
//    double res = 0;
//
//    res = summa(1.0, 2.0, 4.0, 3.0, 5.0, 0.0, 5.5);
//    printf("sum = %lf\n", res);
//
//
//    return 0;
//}
//
//double summa(double one, ...){
//    double sum = one;
//    double value = 0;
//
//    va_list list;
//    va_start(list, one);
//
//    for (int i = 0; ; i++) {
//        value = va_arg(list, double);
//        if (value == 0)
//        {
//            va_end(list);
//            return sum;
//        }
//        sum += value;
//    }
//
//    va_end(list);
//
//    return sum;
//}
