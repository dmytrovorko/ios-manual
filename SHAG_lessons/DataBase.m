//
//  DataBase.m
//  SHAG_lessons
//
//  Created by Дмитрий on 18.05.2018.
//  Copyright © 2018 Vara. All rights reserved.
//

#import <Foundation/Foundation.h>








// ##################################################################################################################################
#pragma mark - KVC, KVO lesson 40
/*
 сылка на операнды
 https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/KeyValueCoding/CollectionOperators.html#//apple_ref/doc/uid/20002176-BAJEAIEE
 
 Aggregation Operators
 
 Aggregation operators work on either an array or set of properties, producing a single value that reflects some aspect of the collection.
 
 @avg
 
 When you specify the @avg operator, valueForKeyPath: reads the property specified by the right key path for each element of the collection, converts it to a double (substituting 0 for nil values), and computes the arithmetic average of these. It then returns the result stored in an NSNumber instance.
 
 To obtain the average transaction amount among the sample data in Table 4-1:
 
 NSNumber *transactionAverage = [self.transactions valueForKeyPath:@"@avg.amount"];
 The formatted result of transactionAverage is $456.54.
 
 @count
 
 When you specify the @count operator, valueForKeyPath: returns the number of objects in the collection in an NSNumber instance. The right key path, if present, is ignored.
 
 To obtain the number of Transaction objects in transactions:
 
 NSNumber *numberOfTransactions = [self.transactions valueForKeyPath:@"@count"];
 The value of numberOfTransactions is 13.
 
 @max
 
 When you specify the @max operator, valueForKeyPath: searches among the collection entries named by the right key path and returns the largest one. The search conducts comparisons using the compare: method, as defined by many Foundation classes, such as the NSNumber class. Therefore, the property indicated by the right key path must hold an object that responds meaningfully to this message. The search ignores nil valued collection entries.
 
 To obtain the maximum of the date values, which is the date of the latest transaction, among the transactions listed in Table 4-1:
 
 NSDate *latestDate = [self.transactions valueForKeyPath:@"@max.date"];
 The formatted latestDate value is Jul 15, 2016.
 
 @min
 
 When you specify the @min operator, valueForKeyPath: searches among the collection entries named by the right key path and returns the smallest one. The search conducts comparisons using the compare: method, as defined by many Foundation classes, such as the NSNumber class. Therefore, the property indicated by the right key path must hold an object that responds meaningfully to this message. The search ignores nil valued collection entries.
 
 To obtain the minimum of the date values, which is the date of the earliest transaction, among the transactions listed in Table 4-1:
 
 NSDate *earliestDate = [self.transactions valueForKeyPath:@"@min.date"];
 The formatted earliestDate value is Dec 1, 2015.
 
 @sum
 
 When you specify the @sum operator, valueForKeyPath: reads the property specified by the right key path for each element of the collection, converts it to a double (substituting 0 for nil values), and computes the sum of these. It then returns the result stored in an NSNumber instance.
 
 To obtain the sum of the transactions amount among the sample data in Table 4-1:
 
 NSNumber *amountSum = [self.transactions valueForKeyPath:@"@sum.amount"];
 The formatted result of amountSum is $5,935.00.
 
 Array Operators
 
 The array operators cause valueForKeyPath: to return an array of objects corresponding to a particular set of the objects indicated by the right key path.
 
 IMPORTANT
 
 The valueForKeyPath: method raises an exception if any of the leaf objects is nil when using array operators.
 
 @distinctUnionOfObjects
 
 When you specify the @distinctUnionOfObjects operator, valueForKeyPath: creates and returns an array containing the distinct objects of the collection corresponding to the property specified by the right key path.
 
 To obtain a collection of payee property values for the transactions in transactions with duplicate values omitted:
 
 NSArray *distinctPayees = [self.transactions valueForKeyPath:@"@distinctUnionOfObjects.payee"];
 The resulting distinctPayees array contains one instance each of the following strings: Car Loan, General Cable, Animal Hospital, Green Power, Mortgage.
 
 NOTE
 
 The @unionOfObjects operator provides similar behavior, but without removing duplicate objects.
 
 @unionOfObjects
 
 When you specify the @unionOfObjects operator, valueForKeyPath: creates and returns an array containing all the objects of the collection corresponding to property specified by the right key path. Unlike @distinctUnionOfObjects, duplicate objects are not removed.
 
 To obtain a collection of payee property values for the transactions in transactions:
 
 NSArray *payees = [self.transactions valueForKeyPath:@"@unionOfObjects.payee"];
 The resulting payees array contains the following strings: Green Power, Green Power, Green Power, Car Loan, Car Loan, Car Loan, General Cable, General Cable, General Cable, Mortgage, Mortgage, Mortgage, Animal Hospital. Note the duplicates.
 
 NOTE
 
 The @distinctUnionOfArrays operator is similar, but removes duplicate objects.
 
 Nesting Operators
 
 The nesting operators operate on nested collections, where each entry of the collection itself contains a collection.
 
 IMPORTANT
 
 The valueForKeyPath: method raises an exception if any of the leaf objects is nil when using nesting operators.
 
 For the descriptions that follow, consider a second array of data called moreTransactions, populated with the data in Table 4-2, and collected together with the original transactions array (from the Sample Data section) into a nested array:
 
 NSArray* moreTransactions = @[<# transaction data #>];
 NSArray* arrayOfArrays = @[self.transactions, moreTransactions];
 Table 4-2Hypothetical Transaction data in the moreTransactions array
 payee values
 amount values formatted as currency
 date values formatted as month day, year
 General Cable - Cottage
 $120.00
 Dec 18, 2015
 General Cable - Cottage
 $155.00
 Jan 9, 2016
 General Cable - Cottage
 $120.00
 Dec 1, 2016
 Second Mortgage
 $1,250.00
 Nov 15, 2016
 Second Mortgage
 $1,250.00
 Sep 20, 2016
 Second Mortgage
 $1,250.00
 Feb 12, 2016
 Hobby Shop
 $600.00
 Jun 14, 2016
 @distinctUnionOfArrays
 
 When you specify the @distinctUnionOfArrays operator, valueForKeyPath: creates and returns an array containing the distinct objects of the combination of all the collections corresponding to the property specified by the right key path.
 
 To obtain the distinct values of the payee property among all the arrays in arrayOfArrays:
 
 NSArray *collectedDistinctPayees = [arrayOfArrays valueForKeyPath:@"@distinctUnionOfArrays.payee"];
 The resulting collectedDistinctPayees array contains the following values: Hobby Shop, Mortgage, Animal Hospital, Second Mortgage, Car Loan, General Cable - Cottage, General Cable, Green Power.
 
 NOTE
 
 The @unionOfArrays operator is similar, but does not remove duplicate objects.
 
 @unionOfArrays
 
 When you specify the @unionOfArrays operator, valueForKeyPath: creates and returns an array containing the all the objects of the combination of all the collections corresponding to the property specified by the right key path, without removing duplicates.
 
 To obtain the values of the payee property in all the arrays within arrayOfArrays:
 
 NSArray *collectedPayees = [arrayOfArrays valueForKeyPath:@"@unionOfArrays.payee"];
 The resulting collectedPayees array contains the following values: Green Power, Green Power, Green Power, Car Loan, Car Loan, Car Loan, General Cable, General Cable, General Cable, Mortgage, Mortgage, Mortgage, Animal Hospital, General Cable - Cottage, General Cable - Cottage, General Cable - Cottage, Second Mortgage, Second Mortgage, Second Mortgage, Hobby Shop.
 
 NOTE
 
 The @distinctUnionOfArrays operator is similar, but removes duplicate objects.
 
 @distinctUnionOfSets
 
 When you specify the @distinctUnionOfSets operator, valueForKeyPath: creates and returns an NSSet object containing the distinct objects of the combination of all the collections corresponding to the property specified by the right key path.
 
 This operator behaves just like @distinctUnionOfArrays, except that it expects an NSSet instance containing NSSet instances of objects rather than an NSArray instance of NSArray instances. Also, it returns an NSSet instance. Assuming the example data had been stored in sets instead of arrays, the example call and results are the same as those shown for @distinctUnionOfArrays.
 
 */

/*
#import "AppDelegate.h"
#import "Student.h"
#import "Group.h"

@interface AppDelegate ()

@property (strong, nonatomic) Student * student;

@property (strong, nonatomic) NSArray * groups;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
//     Student * student = [[Student alloc] init];
//
//
//     student.name = @"Alex";
//     student.age = 20;
//
//     NSLog(@"%@", student);
//
//     [student setValue:@"Roger" forKey:@"name"];
//     [student setValue:[NSNumber numberWithInt:22] forKey:@"age"];
//     NSLog(@"%@", student);
//     NSLog(@"name1 = %@, name2 = %@", [student valueForKey:@"name"], student.name);
//
//     // KVO
//     self.student = student;
//     [self.student addObserver:self
//     forKeyPath:@"name" options: NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
//     context:nil];
//
//     student.name = @"KVONewName";
//
//     [self.student changeName];
//
    
    
    
    Student * student1 = [[Student alloc] init];
    student1.name = @"Alex";
    student1.age = 20;
    
    Student * student2 = [[Student alloc] init];
    student2.name = @"Roger";
    student2.age = 25;
    
    Student * student3 = [[Student alloc] init];
    student3.name = @"Jack";
    student3.age = 22;
    
    Student * student4 = [[Student alloc] init];
    student4.name = @"Vova";
    student4.age = 28;
    
    Group * group1 =[[Group alloc] init];
    group1.students = @[student1, student2, student3, student4];
    
    
//     NSLog(@"%@", group1.students);
//
//     // добавление обсервера в группу. При удалении ниже студента с мютыблэррей, обсервер это отследить в nsArray
//     [group1 addObserver:self
//     forKeyPath:@"students" options: NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
//     context:nil];
//
//     // создание NSMutableArray с NSArray с помощью KVC
//     NSMutableArray * array = [group1 mutableArrayValueForKey: @"students"];
//     [array removeLastObject];
//     NSLog(@"%@", array);
    
    
    
    
//     self.student = student1;
//     // допступ к проперти name у собственного проеперти self.student
//     NSLog(@"name = %@", [self valueForKeyPath:@"student.name"]);
//
//
//     NSString * name = @"Alex";
//     //NSNumber * name = @23;
//     NSError * error = nil;
//     if (![self.student validateValue:&name forKey:@"name" error:&error])
//     {
//     NSLog(@"%@", error);
//     }
    
    
    Student * student5 = [[Student alloc] init];
    student5.name = @"Mitya";
    student5.age = 20;
    
    Student * student6 = [[Student alloc] init];
    student6.name = @"Vova";
    student6.age = 24;
    
    Group * group2 =[[Group alloc] init];
    group2.students = @[student5, student6];
    
    self.groups = @[group1, group2];
    
    // вызов проперти у NSArray count через valueForKeyPath
    NSLog(@"groups count %@", [self valueForKeyPath:@"groups.@count"]);
    
    // объеденеие студентов с помощью KVC. В массиве groups берутся все проперти с key - students и объеденяют в один массив
    // 1 вариант
    NSArray * allStudents = [self valueForKeyPath:@"groups.@distinctUnionOfArrays.students"];
    // 2 варианит
    allStudents = [self.groups valueForKeyPath:@"@distinctUnionOfArrays.students"];
    NSLog(@"%@", allStudents);
    
    
    // операнды @min, @max, @sum, @avg в KVC
    NSNumber * minAge = [allStudents valueForKeyPath:@"@min.age"];
    NSNumber * maxAge = [allStudents valueForKeyPath:@"@max.age"];
    NSNumber * sumAge = [allStudents valueForKeyPath:@"@sum.age"];
    NSNumber * avgAge = [allStudents valueForKeyPath:@"@avg.age"];
    NSLog(@"min age - %@, max age - %@, sum age - %@, avg age - %@", minAge, maxAge, sumAge, avgAge);
    
    NSArray * allNames = [allStudents valueForKeyPath:@"@distinctUnionOfObjects.name"];
    sleep(2);
    NSLog(@"%@", allNames);
    
    
    return YES;
}

#pragma mark - Observing

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"name"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context {
    NSLog(@"\nobserveValueForKeyPath %@\nofObject %@\nchange %@", keyPath, object, change);
}
*/











// ##################################################################################################################################
#pragma mark - CoreData 41 - 44


// -com.apple.CoreData.SQLDebug 1 - добавляем в аргумент Arguments PassedOn Launch - свойство Run, для дебага

// ##################################################################################################################################
#pragma mark - CoreData 41 - 44 class ASDataManager.h

/*
#import "ASDataManager.h"

#import "Student+CoreDataClass.h"
#import "Car+CoreDataClass.h"
#import "University+CoreDataClass.h"
#import "Course+CoreDataClass.h"
#import "Course+FetchedProperties.h"
#import "ASDataManager.h"

@interface ASDataManager ()

@property (nonatomic, strong) NSManagedObjectContext * context;

@end


static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};

@implementation ASDataManager


// создание синглтона
+ (ASDataManager *) sharedManager {
    static ASDataManager * manager = nil;
    
    // создание отдельного потока - защита от того, чтоб данный синглтон не создавали в разных потоках
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ASDataManager alloc] init];
    });
    
    return manager;
}

- (void) generateAndAddUniversity {
    self.context = self.persistentContainer.viewContext;
    
    // список сущностей
    //NSLog(@"%@", [self.persistentContainer.managedObjectModel entitiesByName]) ;
    
    
    // >>>>>>>>>>>>>>>>>>>>> запись в сущность
    
    // взять сущность с базы под названием Student
//    NSManagedObject * student = [NSEntityDescription insertNewObjectForEntityForName:@"Student"
//                                                              inManagedObjectContext:self.persistentContainer.viewContext];
//
//    [student setValue:@"Alex" forKey:@"firstName"];
//    [student setValue:@"Skutarenko" forKey:@"lastName"];
//    [student setValue:[NSDate dateWithTimeIntervalSinceReferenceDate:0] forKey:@"dateOfBirth"];
//    [student setValue:@4 forKey:@"score"];
//
//    NSError * error = nil;
//
//    // запись в базу данных
//    if (![self.persistentContainer.viewContext save: &error]) {
//        NSLog(@"%@", [error localizedDescription]);
//    }
//
    
    
    // <<<<<<<<<<<<<<<<<<<<<<<<<
    
    
    //Student * student = [self addRandomStudent];
    //NSError * error = nil;
    // сохранение созданного студента - или через self property или в самом классе студента, так как он наследник NAManagedObject
    // 1 варианит
    //[student.managedObjectContext save:&error];
    
    // 2 вариант
    // [self.context save:nil];
    
    //    if (error) {
    //        NSLog(@"%@", [error localizedDescription]);
    //    }
    
    
    // >>>>>>>>>>>>>>>>>>>>>>>>> чтение с сущности
    //    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    //
    //    NSEntityDescription * description = [NSEntityDescription entityForName:@"ASObject" inManagedObjectContext:self.persistentContainer.viewContext];
    //
    //    [request setEntity:description];
    
    // возращает в результате массив с числом сколько таких сущностей есть
    //    [request setResultType:NSCountResultType];
    // взращает дикшинари со всеми данным которые в этой сущности
    //[request setResultType:NSDictionaryResultType];
    
    //    NSError * requestErro = nil;
    //    NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
    //    if (requestErro) {
    //        NSLog(@"%@", requestErro);
    //    }
    //
    //    NSLog(@"%@", resultArray);
    
//    for (NSManagedObject * object in resultArray) {
//        NSLog(@"%@ %@ - %@", [object valueForKey:@"firstName"],
//              [object valueForKey:@"lastName"],
//              [object valueForKey:@"score"] );
//    }
    
     
    
    
    //    for (Student * student in resultArray) {
    //        NSLog(@"From class Student: %@ %@ - %lf", student.firstName, student.lastName, student.score);
    //
    //        // удаление объекта
    //        [self.context deleteObject:student];
    //
    //    }
    //    [self.context save:&error];
    
    // <<<<<<<<<<<<<<<<<<<<<<<<<<
    
    
    // #################################################################################################################
    
    //[self deleteAllObjects];
    
    
    // add random student and random car
    
    //
    //    NSError * error = nil;
    //
    //    NSArray * courses = @[[self addCourseWithName:@"iOS"],
    //                          [self addCourseWithName:@"Android"],
    //                          [self addCourseWithName:@"PHP"],
    //                          [self addCourseWithName:@"Javascripy"],
    //                          [self addCourseWithName:@"HTM"]
    //                          ];
    //
    //    University * university = [self addUniversity];
    //
    //    [university addCourses:[NSSet setWithArray:courses]];
    //
    //    for (NSInteger i = 0; i < 15; i++) {
    //        Student * student = [self addRandomStudent];
    //
    //        Car * car = [self addRandomCar];
    //        student.car = car;
    //
    //        //[university addStudentsObject:student];
    //        student.university = university;
    //
    //        NSInteger number = arc4random_uniform(5) + 1;
    //        while (student.courses.count < number) {
    //            Course * course = [courses objectAtIndex:(arc4random_uniform(5))];
    //
    //            if (![student.courses containsObject:course]) {
    //                [student addCoursesObject:course];
    //            }
    //        }
    //
    //    }
    
    
    
    
    
    //    if (![self.persistentContainer.viewContext save:&error])
    //    {
    //        NSLog(@"%@", [error localizedDescription]);
    //    }
    
    
    //[self deleteAllObjects];
    
    // delete Student
    
//    NSFetchRequest * request = [[NSFetchRequest alloc] init];
//
//    NSEntityDescription * description = [NSEntityDescription entityForName:@"Student" inManagedObjectContext: self.context];
//
//    [request setEntity:description];
//
//    NSError * requestErro = nil;
//    NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
//    if ([resultArray count] > 0) {
//        Student * student = [resultArray firstObject];
//        NSLog(@"Student to delete %@", student);
//
//        [self.context deleteObject:student];
//        [self.context save:nil];
//    }
//
    
    // delete University
    
//    NSFetchRequest * request = [[NSFetchRequest alloc] init];
//
//    NSEntityDescription * description = [NSEntityDescription entityForName:@"University" inManagedObjectContext: self.context];
//
//    [request setEntity:description];
//
//    NSError * requestErro = nil;
//    NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
//    if ([resultArray count] > 0) {
//        University * university = [resultArray firstObject];
//
//        [self.context deleteObject:university];
//        [self.context save:nil];
//    }
    
    
    
    
    // delete Car
    
//     NSFetchRequest * request = [[NSFetchRequest alloc] init];
//
//     NSEntityDescription * description = [NSEntityDescription entityForName:@"Car" inManagedObjectContext: self.context];
//
//     [request setEntity:description];
//
//     NSError * requestErro = nil;
//     NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
//     if ([resultArray count] > 0) {
//
//         Car * car = [resultArray firstObject];
//         [self.context deleteObject:car];
//         [self.context save:nil];
//     }
    
    // ################################################################################################################################################################################
    
//     NSFetchRequest * request = [[NSFetchRequest alloc] init];
//
//     NSEntityDescription * description = [NSEntityDescription entityForName:@"Course" inManagedObjectContext: self.context];
//
//     [request setEntity:description];
//
//     // [request setRelationshipKeyPathsForPrefetching:@[@"courses"]];
//
//     // выборка пакетов - будет показано, загружено только 20 строк, а затем начнутся загружаться следущие 20 строк (для того чтоб не загружать весь массив)
//     //[request setFetchBatchSize:20];
//     // ограничение во взятии - максимум 30 студентов
//     //[request setFetchLimit:30];
//     // указывается с какого студента по номеру нужно начинать брать выборку
//     //[request setFetchOffset:10];
//
//     // сортировка массива с помощью Core Dat - сначала по имени, потом по фамилии
//     //NSSortDescriptor * firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
//     //NSSortDescriptor * lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
//     //[request setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
//
//
//
//     // вывод стунетов у которых score больше 3 меньше 3.5, курсов больше 3, и имя содержит имя validNames
//     //    NSArray * validNames = @[@"Roma", @"Denver", @"Norbert"];
//     //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"score > %f && score <= %f && courses.@count >= %li AND firstName IN %@", 3.0, 3.5, 3, validNames];
//
//     // вывод курсов где средняя оценка студентов больеш 3 баллов
//     // NSPredicate * predicate = [NSPredicate predicateWithFormat:@"students.@avg.score > %f", 3.00];
//
//     //[request setPredicate:predicate];
//
//     // сортировка по баллу
//     //NSSortDescriptor * scoreDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"score" ascending:YES];
//     // [request setSortDescriptors:@[scoreDescriptor]];
//
//     // группы где количество студентов с машиной BMW больше на n-ое количество
//     //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(students, $student, $student.car.model == %@).@count >= %li", @"BMW", 11];
//     //    [request setPredicate:predicate];
//
//
//     NSError * requestErro = nil;
//     NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
//
//     //    resultArray = [resultArray subarrayWithRange: NSMakeRange(0, 50)];
//
//     [self printArray:resultArray];
//
//
//     //[self printAllObjects];
    
    
    // #########################################################################################################################
    
    // создание запроса с Fetch, которое создается в кор-дате моделе (add fetch request). Минус этого метода - в такие реквесты нельзя добавлять дескрипторы для сортировки (нужно тольео сделать copy при создании реквеста)
    
//     NSFetchRequest * request = [[self.persistentContainer.managedObjectModel fetchRequestTemplateForName:@"FetchStudents"] copy];
//
//     NSSortDescriptor * firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
//     NSSortDescriptor * lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
//     [request setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
//
//     NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
//
//     [self printArray:resultArray];
    
    
    // #########################################################################################################################
    // обращение к собственному проперти - Fetched Properties.
    // чтоб обратится к собтвенной переменой в предикате Fetched Properties - $FETCHED_SOURCE.name (вводится с файле базы данных, в настройках Fetched Properties)
    
//     NSFetchRequest * request = [[NSFetchRequest alloc] init];
//
//     NSEntityDescription * description = [NSEntityDescription entityForName:@"Course" inManagedObjectContext: self.context];
//
//     [request setEntity:description];
//
//     NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
//
//     for (Course * course in resultArray) {
//     NSLog(@"COURSE NAME - %@", course.name);
//     NSLog(@"BEST STUDENTS:");
//     [self printArray:course.bestStudents];
//     }
//
    
    //[self deleteAllObjects];
    
}


- (void) printArray: (NSArray *) array {
    
    for (id object in array) {
        if ([object isKindOfClass:[Car class]]) {
            Car * car = (Car*) object;
            NSLog(@"CAR: %@, OWNER: %@ %@", car.model, car.owner.firstName, car.owner.lastName);
        } else  if ([object isKindOfClass:[Student class]]) {
            Student * student = (Student*) object;
            NSLog(@"STUDENT: %@ %@,  SCORE - %.2lf COURSES - %li CAR - %@",student.firstName, student.lastName,  student.score, student.courses.count, student.car.model);
        }
        else  if ([object isKindOfClass:[University class]]) {
            University * university = (University*) object;
            NSLog(@"UNIVERSITY: %@, Students: %li",university.name, university.students.count );
        }
        else  if ([object isKindOfClass:[Course class]]) {
            Course * course = (Course*) object;
            
            NSLog(@"COURSE: %@ Students count: %li", course.name, course.students.count);
        }
    }
    NSLog(@"%li", array.count);
}

- (NSArray *) allObjects {
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description = [NSEntityDescription entityForName:@"ASObject" inManagedObjectContext:self.persistentContainer.viewContext];
    
    [request setEntity:description];
    
    NSError * requestErro = nil;
    NSArray * resultArray = [self.persistentContainer.viewContext executeFetchRequest:request error:&requestErro];
    if (requestErro) {
        NSLog(@"%@", requestErro);
    }
    return resultArray;
}

- (void) deleteAllObjects {
    
    NSArray * allObjects = [self allObjects];
    
    NSError * error = nil;
    
    for (id object in allObjects) {
        [self.context deleteObject:object];
    }
    
    [self.context save:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
}

- (void) printAllObjects {
    
    NSArray * allObjects = [self allObjects];
    [self printArray:allObjects];
}

- (University *) addUniversity {
    
    
    University * university = [NSEntityDescription insertNewObjectForEntityForName:@"University"
                                                            inManagedObjectContext:self.persistentContainer.viewContext];
    university.name = @"ONPU";
    return university;
}

- (Course *) addCourseWithName: (NSString *) name {
    
    Course * course = [NSEntityDescription insertNewObjectForEntityForName:@"Course"
                                                    inManagedObjectContext:self.persistentContainer.viewContext];
    course.name = name;
    return course;
}

- (Car *) addRandomCar {
    
    static NSString * carModelNames [] = {
        @"Dodge", @"Toyota", @"BMW", @"Lada", @"Volge"
    };
    
    Car * car = [NSEntityDescription insertNewObjectForEntityForName:@"Car"
                                              inManagedObjectContext:self.persistentContainer.viewContext];
    car.model = carModelNames[arc4random_uniform(5)];
    return car;
}

- (Student*) addRandomStudent {
    
    Student* student =
    [NSEntityDescription insertNewObjectForEntityForName:@"Student"
                                  inManagedObjectContext:self.persistentContainer.viewContext];
    
    student.score = (float)arc4random_uniform(201) / 100.f + 2.f;
    student.dateOfBirth = [NSDate dateWithTimeIntervalSince1970:60 * 60 * 24 * 365 * arc4random_uniform(31)];
    student.firstName = firstNames[arc4random_uniform(50)];
    student.lastName = lastNames[arc4random_uniform(50)];
    
    return student;
}

@synthesize persistentContainer = _persistentContainer;


#pragma mark - Core Data stack


- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"CoreDataTestSkutarenko1"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end


*/





#pragma mark - CoreData 41 - 44 class CoreDataViewController.h

/*
#import "CoreDataViewController.h"
#import "ASDataManager.h"

@interface CoreDataViewController ()

@end

@implementation CoreDataViewController

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (NSManagedObjectContext *)managedObjectContext {
    if (!_managedObjectContext) {
        _managedObjectContext = [[ASDataManager sharedManager] persistentContainer].viewContext;
    }
    return _managedObjectContext;
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
    }
}





#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    return nil;
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath *) indexPath {
    
}



 }
 
@end
 */




#pragma mark - CoreData 41 - 44 class UniversitiesViewController


/*
#import "UniversitiesViewController.h"
#import "University+CoreDataClass.h"
#import "CoursesViewController.h"

@interface UniversitiesViewController ()

@end

@implementation UniversitiesViewController
@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Universities";
    
}


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description = [NSEntityDescription entityForName:@"University"
                                                    inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescriptor]];
    
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
                                                             initWithFetchRequest:fetchRequest
                                                             managedObjectContext:self.managedObjectContext
                                                             sectionNameKeyPath:nil
                                                             cacheName:@"Master"];     // имя файла КЭША - на жестком диски для CoreData. Ускорят запрос если запрашиваем одно и тоже.
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    University * university = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = university.name;
    cell.detailTextLabel.text = nil;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    University * university = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CoursesViewController * vc = [[CoursesViewController alloc] init];
    vc.university = university;
    
    [self.navigationController pushViewController:vc animated:YES];
}
 
 */




















