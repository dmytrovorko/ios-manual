//
//  Other Фичи.m
//  SHAG_lessons
//
//  Created by Дмитрий on 06.05.2018.
//  Copyright © 2018 Vara. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark - создание собственного NSLog (сишная функция)
/*
void ASLog(NSString * format, ...) {
    
#if LOGS_ENABLED            // выводить или не выводить в консоль текст
    
    va_list argumentList;
    va_start (argumentList, format);
    NSLogv(format, argumentList);
    
    
#if LOGS_NOTFOCATIONS_ENABLED  // создание нотификации, для отслеживания ASLoga и вывода их в консоль (textView) приложения
    NSString * log = [[NSString alloc] initWithFormat:format arguments:argumentList];
    [[NSNotificationCenter defaultCenter] postNotificationName:ASLogNotification
                                                        object:nil
                                                      userInfo:@{ASLogNotificationTextUserInfoKey: log}];
#endif
    va_end(argumentList);
    
#endif
}
 */


#pragma mark - Установить какое то свойство по умолчанию ( UI_APPEARANCE_SELECTOR )

// устанавливает цвет надписи кнопок в красный цвет по умолчанию
//[[UIButton appearance] setTitleColor:RGBA(255, 0, 0, 255) forState:UIControlStateNormal];

// устанавливает цвет в красный при условии что кнопка лежит на классе UIView
//[[UIButton appearanceWhenContainedInInstancesOfClasses:@[[UIView class]] ] setTitleColor:RGBA(255, 0, 0, 255) forState:UIControlStateNormal];




#pragma mark - Создание Синглтона
/*
// создание синглтона
+ (ASDataManager *) sharedManager {
    static ASDataManager * manager = nil;
    
    
    // создание отдельного потока - защита от того, чтоб данный синглтон не создавали в разных потоках
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ASDataManager alloc] init];
    });
    
    return manager;
}
 */

#pragma mark - путь к файлу который находится в папке приложения
/*
NSString * filePath = [[NSBundle mainBundle] pathForResource:@"pdf33.pdf" ofType:nil];
*/

#pragma mark - Проверка типа гаджета - iPhone, iPad
/*
UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
*/
#pragma mark - Создание бэкграунд потока! И в том же потоке доступ к мэйн потоку (через dispatch)
/*
__weak ViewController * weakSelf = self;



[self.operationsQueue cancelAllOperations];

self.operationsQueue = [[NSOperationQueue alloc] init];


NSLog(@"Number of operations  - %li", self.operationsQueue.operationCount);
DYMyOperation * operation = [DYMyOperation operationWithBlock:^{
    NSArray * tempArray = [weakSelf sortStudents:studentsArray sortCompare:sortCompare withStringFilter: filter];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.sections = tempArray;
        [weakSelf.tableView reloadData];
        
    }) ;
}
                                                    andNumber:1];


[self.operationsQueue addOperation:operation];
 */


#pragma mark - Сортировка строк в массиве, используя Descriptor
/*
NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
[array sortUsingDescriptors:@[sortDescriptor]];
*/

#pragma mark - всплывающие системные окна с кнопкой
/*
UIAlertController * alert = [UIAlertController
                             alertControllerWithTitle:@"Title"
                             message:@"Message"
                             preferredStyle:UIAlertControllerStyleAlert];



UIAlertAction* yesButton = [UIAlertAction
                            actionWithTitle:@"Yes, please"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action) {
                                //Handle your yes please button action here
                            }];

UIAlertAction* noButton = [UIAlertAction
                           actionWithTitle:@"No, thanks"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               //Handle no, thanks button
                           }];

[alert addAction:yesButton];
[alert addAction:noButton];

[self presentViewController:alert animated:YES completion:nil];
 */

#pragma mark - Перевод килобайтов в байты, мб, гб и т.д.

/*
- (NSString *) fileSizeFromValue: (unsigned long long) size {
    static NSString * units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    static unsigned unitsCount = 5;
    int index = 0;
    double fileSize = (double) size;
    
    while (fileSize > 1024 && index < unitsCount) {
        fileSize = fileSize / 1024;
        index++;
    }
    return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
}
*/
#pragma mark - Игнорирование всех тачей приложения
//[[UIApplication sharedApplication] beginIgnoringInteractionEvents];

#pragma mark - При Debug команда строки
/*
 
 //вывод значение текущих объектов
po _вводим_интересующий обьект_ => жмем Enter (и снизу выскакивает ее(его) значение (то что прописано у обьекта в дескрипшине) )
 
 //вывод значение текущих переменных
 p _вводим_интересующий переменную => жмем Enter (и снизу выскакивает ее(его) значение )
*/


#pragma mark - Добавление UINavigationController через код в appDelegate

/*
#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController: self.window.rootViewController];
    self.window.rootViewController = navController;
    
    return YES;
}
*/
