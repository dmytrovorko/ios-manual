//
//  Git.m
//  SHAG_lessons
//
//  Created by Дмитрий on 25.03.2018.
//  Copyright © 2018 Vara. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark SOFT
// WebStorm - утилита для редоктирования конфликтов при merge
// Bitbacket - сервис с работой удаленного репозитория Bithub (логин с гугла)
// https://www.gitignore.io - ресурс для создания файла .gitignore

#pragma mark Teory
//      SHA-1 - метод создания хэш алгоритма, для идентификации репозитория. Нужень для того, чтоб каждые изменения в рабоче директории имело отличное от основного репозитория хэш. 40 - символов, в шестнадцати ричном представлении.
// HEAD - указатель на конкретный комит в репозитории. Указывает на верхушку текущего бранча (branch - ветка) репозитория. Указывает на место, где мы будет начинать следующую запись.
// Длина massege должна быть первая строка - не более 52 символа, вторая строка не более 72 символов

#pragma mark commands in terminal
/*
 //общие
 ls                     //просмотр содержимоого текущего католога (без скрытых файлов)
 ls -la                    //просмотр содержимоого текущего католога, в т.ч. скрытые файлы
 cd путь_к_директории   //переход к директории (или название папки)
 cd ..                  //возврат на папку назад
 nano "название файла"  //открыть файл с помощью nano редактора
 
*/
#pragma mark initalized / commit
/*
 cd "/Users/dmitriy/Document/First git project"        //переход в директория для отслеживания
 git project dmitriy$ git init                                           //создание репозитория в данной директории (инициализация)
    Initialized empty Git repository in /Users/dmitriy/Document/First git project/.git/               // .git - папка програмы git в которой она работает (репозиторий). Там ничего не трокаем, кроме файла config (конфигурация)

 
 git add .                                          // подготовить изменения
 git commit -m "Initial commit"                     // сделать commit (локальное сохранение изменений). -m(massege) - подпись коммита
    [master (root-commit) 5415926] Initial commit                                                     //информация про commit
    2 files changed, 2 insertions(+)
    create mode 100644 Any file
    create mode 100644 My first commit
 
 */
 
 
#pragma mark general commands
/*
 git status             //просмотра статуса файлов в рабочей директории - ветка на котором сейчас находится репозитив и файлы которые подлижат commitу, если таковы имеются
 which git              //дериктория установки программы
 git --version          //версия установленного git
 git help               //вызов информации как использовать git, его команды
 git help log           // Git manual. информация отккрывается в новой странице (чтоб выйти нажать q (выйти)
 git add .              // подготовить изменения - добавить в буфер. Удобно если у вас 10 изменений, но вы хотите добавть только 5. Добавляете пять изменения в буфер, а потом комитите
 git commit -m "Initial commit"   // сделать commit (локальное сохранение изменений). -m(massege) - подпись коммита. Длина massege должна быть первая строка - не более 52 символа, вторая строка не более 72 символов
 
 git rese "file_name"       //сброс с буфера
*/
 
 
 #pragma mark checkout
 /*
 git checkout e3d0caf   //чекаут по хэшу
 git reset HEAD index.txt   //достать проиндексированый файл с буфера
 git revert HEAD --no-edit   //(вернуться)возвращение к предыдущему комиту. (Эта команда не удаляет комит, а просто откатывается назад  и убдет в логе написано Revert
 git revert --abort         //отмена revert
 */
 
 
 #pragma mark branch
/*
 git brench -       //список всех бренчей
 git checkout -b newbranch   //создать новую ветку newbranch
 git checkout newbranch      //переместиться в ветку newbranch
 */

 
 
 #pragma mark merge
/*
git merge anyBranch                             // слияние текущей ветви с anyBranch
git commit -am "Commit after merge conflict"   // -am - используется при комите после решения конфликта (по другому не закомитися)
*/

#pragma mark rebase (переопределить)
/*
 git rebase anyBanch            //слияние текущей ветки с anyBanch.  - рекомендуется для использования своих локальных веток. Для публичных веток использовать merge
 
 */
 
 

#pragma mark view commits (log)
 /*
 git project dmitriy$ git log                                            //просмотр коммитов
    commit 54159261c02c0806fa4835aeac89a12bd23c24a5 (HEAD -> master)              // 54159261c02c0806fa4835aeac89a12bd23c24a5 - хэш,уникальное id
    Author: Dmytro Vorko <dimavorko88@gmail.com>
    Date:   Sun Mar 25 23:58:51 2018 +0300
    Initial commit                                                                //многострочное сообщение комита


 git log                // просмотр коммитов
 git log -n 1           //вызов одного комита
 git log --since=2018-03-25 //вывод комита за данную дату
 git log --until=2018-03-26 //вывод комита до данной даты
 git log --author="Dmytro"  //вывод комита с автором включающую данную строку
 git log --grep="Init"      //поиск с регулярным выражением
 git log HEAD           //посмотреть на текущий HEAD
 git log --pretty=format:'%h %ad | %s%d' --date=short     // удобная, понятная форма просмотров коммитов, с хэшом и датой
 
 
 
 */

//################################################################################################################################
#pragma mark setting of git client
/*
 
 Распространение настроек:
 - системная конфигурация (в конце --system)
    git config --system
 - пользовательский уровень (в конце --global)
    git config --global
 - для отдельного проекта (в конце ничего)
    git config
 
git config --global user.name "Dmytro Vorko"                //добавление имени пользователя
git config --global user.email "dimavorko88@gmail.com"      //добавление эмейла пользователя
git config --list                                           //список конфигураций
git config user.name                                        //просмотр свойства user.name
git config --global color.ui true                           //установка подсветки текста в терминале в зависимости от важности и т.д.
 
.gitconfig - файл с настройками в папке ~
 .gitignore - файл который нужно создать для игнора в папке с репозиторием
 
 //просмотр конфигурация (следущие три команды):
 MacBook-Air-Dmitrij:~ dmitriy$ cd ~
 MacBook-Air-Dmitrij:~ dmitriy$ ls -la
 MacBook-Air-Dmitrij:~ dmitriy$ cat .gitconfig
 */
 //################################################################################################################################

#pragma mark initalizing tracking
// инициализация отслеживания
 /*
  
  MacBook-Air-Dmitrij:Calculator IOS dmitriy$ cd "/Users/dmitriy/Document/First git project"        //переход в директория для отслеживания
  MacBook-Air-Dmitrij:First git project dmitriy$ git init                                           //создание репозитория в данной директории (инициализация)
  Initialized empty Git repository in /Users/dmitriy/Document/First git project/.git/               // .git - папка програмы git в которой она работает (репозиторий). Там ничего не трокаем, кроме файла config (конфигурация)
 
  ############################
  
  MacBook-Air-Dmitrij:First git project dmitriy$ git add .                                          // подготовить изменения - добавить в буфер. Удобно если у вас 10 изменений, но вы хотите добавть только 5. Добавляете пять изменения в буфер, а потом комитите
  MacBook-Air-Dmitrij:First git project dmitriy$ git commit -m "Initial commit"                     // сделать commit (локальное сохранение изменений). -m(massege) - подпись коммита
  [master (root-commit) 5415926] Initial commit                                                     //информация про commit
  2 files changed, 2 insertions(+)
  create mode 100644 Any file
  create mode 100644 My first commit
  
  
  
  MacBook-Air-Dmitrij:First git project dmitriy$ git log                                            //просмотр коммитов
  commit 54159261c02c0806fa4835aeac89a12bd23c24a5 (HEAD -> master)              // 54159261c02c0806fa4835aeac89a12bd23c24a5 - хэш,
                                                                                //уникальное id
  Author: Dmytro Vorko <dimavorko88@gmail.com>
  Date:   Sun Mar 25 23:58:51 2018 +0300
  
  Initial commit                                                                //многострочное сообщение комита

 */


 #pragma mark удаленный репозиторий
/*
 
 git remote                                     //просмотреть удаленный репозиторий (по умолчанию присваивается название origin)
 
 git remote -v                                  //просмтреть сылки на удаленный репозиторий
 
 git clone https://адрес_удаленного_репозитория   //клонирование удаленного репозитория в текущую директорию локально
 
 git remote add origin https://adress.git           //добавление rotate к текущему репозиторию

 git push -u origin master                         //пуш локального репозитория на удаленный
 
 git remote remove origin                           //удаление удаленного репозитория
 
 git push origin newbranch                          //залить в origin (считается сылка на удаленный доступ) новую ветку (так как ее до этого не было) по названием newbanch (новую в том случае если такой не было.. если есть, то добавит новые изменения в текущую ветку)
 
 git pull origin master                             //скачать текущий последний комит с удаленного репозитория ветки master
 
 git pull --rebase origin master                    // работает так же как и рэбэйс
 
 git stash       // отложить текущие локальные изменения, для того чтоб допустим выкачать какие то комиты с удаленного рпозитория, а потом командой git stash pop вернуть их и там уже
                  //принимать решения комитить эти изменния или нет. Стэшей может быть несколько
 
 git stash pop                      // вернуть изменения которые мы отложивали с помощью git stash. Загружает послдений стэш со списка (если их несколько) и удаляет его
 
 git stash list                        // просмотреть список стэшей - количество изменение в корзине
 
 git stash show                     //просмтреть последнее сохраненное изменение в корзине
 
 git stash apply                    //добавить с корзины последнее сохраненное туда изменение (но не удаляет его из списка)
 
 git stash drop                     //удаление последнего добавленного stash из списка
 
 git stash clear                    //очистка всех стэшей
 
 
 
 
 */
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


